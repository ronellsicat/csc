function out_data = ConcatenateData(in_data, target_size, tile_size, image_boundary, image_names)

    sz = size(in_data);
    sz(1,1) = target_size(1,1) + (image_boundary(1,1)*2);
    sz(1,2) = target_size(1,2) + (image_boundary(1,2)*2);
    out_data = zeros(sz(:,1:end-1));  

    num_tiles = ceil(target_size ./ tile_size);
    num_slices = 1;
    if(length(size(out_data)) == 3)
       num_slices = size(out_data,3); 
    end
    
    tile_order = 1;
    for r = 1 : num_tiles(1,1)
        for c = 1 : num_tiles(1,2)

            tile_upper_left = [image_boundary(1,1) + ((r-1) * tile_size(1,1)) + 1, ...
                    image_boundary(1,2) + ((c-1)* tile_size(1,2)) + 1];

            row_start = tile_upper_left(1);
            row_end = min(size(out_data, 1), row_start + tile_size(1)-1);

            col_start = tile_upper_left(2);
            col_end = min(size(out_data, 2), col_start + tile_size(2)-1);

            % We need this here because CSC unorders the tiles when loading
            % them from disk. This scheme assumes that the tiles are
            % properly linearly numbred from left to right and top to
            % bottom.
            tile_index = GetNameIndex(image_names, tile_order);

            if(num_slices == 1)

                 sub_matrix = in_data(image_boundary(1,1)+1:image_boundary(1,1)+tile_size(1,1),...
                    image_boundary(1,2)+1:image_boundary(1,2)+tile_size(1,2), tile_index);

                out_data(row_start:row_end, col_start:col_end) = sub_matrix;
                 tile_order = tile_order + 1;   
            else
                sub_matrix = in_data(image_boundary(1,1)+1:image_boundary(1,1)+tile_size(1,1),...
                    image_boundary(1,2)+1:image_boundary(1,2)+tile_size(1,2), :, tile_index);

                out_data(row_start:row_end, col_start:col_end, :) = sub_matrix;
                 tile_order = tile_order + 1;   
            end
        end
    end

end


function index = GetNameIndex(names_list, order)

    assert(length(names_list) > 0);
    for ii = 1: length(names_list)
       
        [pathstr,name,ext] = fileparts(names_list{1,ii});
        
        if(str2num(name) == order)
           index = ii;
           return;
        end
        
    end
end