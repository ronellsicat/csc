function output_histogram = compute_dictionary_histogram_for_tile(fmaps, image_index, ...
tile_upper_left, tile_size, coeff_threshold_min, coeff_threshold_max)

    num_atoms = size(fmaps, 3);
    output_histogram = zeros(num_atoms, 1);
    %fprintf('\nComputing coefficient histogram for %d atoms.\n', num_atoms);

    for atom = 1 : num_atoms
        
        row_start = tile_upper_left(1);
        row_end = min(size(fmaps, 1), row_start + tile_size(1)-1);
        
        col_start = tile_upper_left(2);
        col_end = min(size(fmaps, 2), col_start + tile_size(2)-1);
        
        submatrix = fmaps( row_start:row_end, col_start:col_end, atom, image_index);
        
       output_histogram(atom) = sum( ((abs( submatrix(:) ) >= coeff_threshold_min) + ...
           (abs( submatrix(:) ) <= coeff_threshold_max)) == 2);
    end
end % function