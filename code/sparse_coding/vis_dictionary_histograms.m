function [coeffs_histograms, tile_labels, total_counted_coeffs_per_image] = vis_dictionary_histograms(vis_type, dictionary, fmaps, tile_size, ...
    coeff_threshold_min, coeff_threshold_max, num_coeff_bins, plot_max_y, plots_dir)
    
    use_matrix_vis = strcmp(vis_type, 'matrix')
    use_stacked_bar = (num_coeff_bins > 1);
    opengl('save', 'software');
    coeffs_histograms = [];
    tile_labels = {};
    
    % TEMP:
    atom_size = [size(dictionary, 1), size(dictionary,2)];
    image_boundary = [floor(atom_size(1)/2)*2, floor(atom_size(2)/2)*2];
    
    max_coeffs_per_tile = tile_size(1) * tile_size(2);
    sparsity = 0.25; % Assumption that sparisty percent of coeffs are 0. This is used to control the max y value of histograms.
    
    if(plot_max_y == 0)
        plot_max_y = floor(max_coeffs_per_tile * sparsity); 
    end
    
    %fprintf('\nShowing dictionary histogram for %d atoms.\n', size(dictionary, 3));
    
    num_images = size(fmaps, 4);
    images_size = [size(fmaps, 1) - image_boundary(1), ...
        size(fmaps, 2) -  image_boundary(2)];
    
    coeff_bin_size = (coeff_threshold_max - coeff_threshold_min) / num_coeff_bins;
    num_atoms = size(dictionary, 3);

    total_counted_coeffs_per_image = zeros(num_images,1);

        
    % Go through each image in the database; for each image create a figure:
    % go through each tile and compute a histogram to display for the tile.
    
    for image_index = 1 : num_images
       
        % Compute number of tiles to compute histogram for.
        num_tiles = [ceil(images_size(1) / tile_size(1)), ceil(images_size(2) / tile_size(2))];
        
        % Create a plot for the image.
        hist_image = figure('units','normalized','outerposition',[0 0 1 1]);
        title_var = sprintf('Coeff Histogram for Image: %d', image_index);
        set(gcf,'name',title_var,'numbertitle','off')
        
        tile_index = 1;
        for tr = 1 : num_tiles(1) % Go through each row
            for tc = 1 : num_tiles(2) % Go through each col
                
                %subplot(num_tiles(1), num_tiles(2), tile_index);
                subplot_tight(num_tiles(1), num_tiles(2), tile_index, [0.01, 0.01]);
                
                tile_labels = [tile_labels; sprintf('(%d, %d, %d)', image_index, tr, tc)];
                
                tile_upper_left = [image_boundary(1) + ((tr-1) * tile_size(1)) + 1, ...
                    image_boundary(2) + ((tc-1)* tile_size(2)) + 1];
                
                if(use_matrix_vis == 1)
                    
                    histogram_linear = compute_dictionary_histogram_for_tile( ...
                        fmaps, image_index, tile_upper_left, tile_size, coeff_threshold_min, coeff_threshold_max);
                    
                    coeffs_histograms = [coeffs_histograms; histogram_linear'];
                    total_counted_coeffs_per_image(image_index, 1) = total_counted_coeffs_per_image(image_index, 1) + sum(histogram_linear); 
                    
                    num_atoms_in_col = ceil(sqrt(size(dictionary,3)))
                    num_atoms_in_row = ceil(num_atoms / num_atoms_in_col)
                    
                    matrix_hist = reshape(histogram_linear, [num_atoms_in_row, num_atoms_in_col]);
                    size_scaling = 10;
                    matrix_hist = imresize(matrix_hist, size_scaling, 'nearest');
                   
                    matrix_hist = adjust_region_boundaries(matrix_hist, ...
                        [size_scaling, size_scaling], plot_max_y);
                 
                    imshow(matrix_hist, 'Colormap', gray, ...
                        'DisplayRange', [0, plot_max_y]);
                    colorbar('southoutside');
                 
            
                elseif(use_stacked_bar == 1)
                    
                    histogram_stacked = zeros(num_atoms, num_coeff_bins);

                    for bin = 1 : num_coeff_bins

                        bin_min = ((bin - 1) * coeff_bin_size) + coeff_threshold_min;
                        bin_max = bin_min + coeff_bin_size;
                        histogram_linear = compute_dictionary_histogram_for_tile( ...
                            fmaps, image_index, tile_upper_left, tile_size, bin_min, bin_max);

                        coeffs_histograms = [coeffs_histograms; histogram_linear'];
                        total_counted_coeffs_per_image(image_index, 1) = total_counted_coeffs_per_image(image_index, 1) + sum(histogram_linear);
                        
                        histogram_stacked(:, bin) = histogram_linear;
                    end

                    bar(histogram_stacked, 'stacked');
                    ylim([0 plot_max_y]);
                
                else
                    
                    histogram_linear = compute_dictionary_histogram_for_tile( ...
                        fmaps, image_index, tile_upper_left, tile_size, coeff_threshold_min, coeff_threshold_max);

                    coeffs_histograms = [coeffs_histograms; histogram_linear'];
                    total_counted_coeffs_per_image(image_index, 1) = total_counted_coeffs_per_image(image_index, 1) + sum(histogram_linear);
                    
                    bar(histogram_linear);
                    ylim([0 plot_max_y]);
                end
                
                
                
                tile_index = tile_index + 1;
            end
        end
        
        if(use_stacked_bar == 1)
            
            % Create legend:
            legend_labels = cell(1, num_coeff_bins);
            for bin = 1 : num_coeff_bins

                bin_min = ((bin - 1) * coeff_bin_size) + coeff_threshold_min;
                bin_max = bin_min + coeff_bin_size;

                legend_labels{1, bin} = sprintf('%f-%f', bin_min, bin_max);
            end

            legend(legend_labels,'Location','NorthWest');
        end
    end
    
    saveas(gcf, sprintf('%splots_dictionary_histogram_%d_num_coeffs_%d.png', plots_dir, image_index, total_counted_coeffs_per_image(image_index, 1)));
    
    size(coeffs_histograms)
end % function




% TODO: Adjust boundary vis
function adjusted = adjust_region_boundaries(input_region, region_block_size, ...
    boundary_value)

    adjusted = input_region;
    num_cols = floor(size(input_region,2) / region_block_size(2));
    num_rows = floor(size(input_region,1) / region_block_size(1));
    cur_col_index = region_block_size(2);
    adjusted(:, cur_col_index) = boundary_value;
    for c = 1 : num_cols-1
        
        cur_col_index = cur_col_index + region_block_size(2);
        adjusted(:, cur_col_index) = boundary_value;
    end
    
    cur_row_index = region_block_size(1);
    adjusted(cur_row_index, :) = boundary_value;
    for r = 1 : num_rows-1
        
        cur_row_index = cur_row_index + region_block_size(1);
        adjusted(cur_row_index, :) = boundary_value;
    end
    
end