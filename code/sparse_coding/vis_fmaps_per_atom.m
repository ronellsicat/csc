function feature_maps_sum_count = vis_fmaps_per_atom(fmaps, tile_size, coeff_threshold_min, coeff_threshold_max, show_binary_maps, vis_type, plots_dir)
    
%% TODO: Needs cleanup.

 %Display range of a grayscale image, specified as a two-element vector of the form [low high]. The imshow function displays the value low (and any value less than low) as black, and it displays the value high (and any value greater than high) as white. Values between low and high are displayed as intermediate shades of gray, using the default number of gray levels. If you specify an empty matrix ([]), imshow uses [min(I(:)) max(I(:))]. In other words, use the minimum value in I as black, and the maximum value as white.

    atoms_fmap_dir = strcat(plots_dir, '/atoms_fmaps/');
    if ~exist(atoms_fmap_dir, 'dir')
        mkdir(atoms_fmap_dir);        
    end

    use_stacked_images = (strcmp(vis_type, 'stacked') == 1);
           
    num_images = size(fmaps, 4);
    num_atoms = size(fmaps, 3);
    
    num_atoms_in_col = ceil(sqrt(num_atoms));
    num_atoms_in_row = ceil(num_atoms / num_atoms_in_col);
    
    for image_index = 1 : num_images
    
        feature_maps_sum_count = zeros(size(fmaps, 1), size(fmaps, 2));
        feature_maps_sum_abs_coeff = zeros(size(fmaps, 1), size(fmaps, 2));
        
        figure('units','normalized','outerposition',[0 0 1 1]);
        
        image_stack = 0;
        if(use_stacked_images)
           image_stack = zeros(size(fmaps, 1), size(fmaps, 2), num_atoms);
        end
        
        for atom_index = 1 : num_atoms

            binary_fmap_thresholded = ((abs(fmaps(:,:, atom_index, image_index)) >= coeff_threshold_min) + ...
                (abs(fmaps(:,:, atom_index, image_index)) <= coeff_threshold_max)) == 2;
            
            feature_maps_sum_count = feature_maps_sum_count + binary_fmap_thresholded;
            
            if(show_binary_maps)
                
                if(use_stacked_images)
                    image_stack(:,:,atom_index) = binary_fmap_thresholded;
                else
                    %fprintf('Showing binary mask.\n');
                    subplot_tight(num_atoms_in_row, num_atoms_in_col, atom_index, [0.01, 0.01]);
                    imshow(binary_fmap_thresholded, 'Colormap', gray, ...
                        'DisplayRange', [0, 1]);
                    axis equal;
                    axis tight;
                    
                   imwrite(binary_fmap_thresholded*255, sprintf('%sfmaps_per_atom_im%d_atom%d.png', atoms_fmap_dir, image_index, atom_index));
                end
               
            else
                
                fmap_thresholded = abs(fmaps(:,:, atom_index, image_index)) .* binary_fmap_thresholded;
                feature_maps_sum_abs_coeff = feature_maps_sum_abs_coeff + fmap_thresholded;
                
                if(use_stacked_images)
                    image_stack(:,:,atom_index) = fmap_thresholded;
               
                else
                    subplot_tight(num_atoms_in_row, num_atoms_in_col, atom_index, [0.01, 0.01]);
                    imshow(abs(fmaps(:,:, atom_index, image_index)), 'Colormap', gray, ...
                        'DisplayRange', [coeff_threshold_min, coeff_threshold_max]);
                    colorbar('southoutside');  
                    axis equal;
                    axis tight;
                   
                end
            end
        end
        
        if(use_stacked_images)
               
            imtool3D(image_stack);
        end
        
        saveas(gcf,sprintf('%splots_fmaps_per_atom_%d.png', plots_dir, image_index));
        
        figure();
        max_count = max(feature_maps_sum_count(:));

        % COmputing the sum or count of coeffs here may produce higher
        % results than correct because we should consider the boundary
        % regions of the feature maps which has coefficients.
        %
        %sum_count = sum(feature_maps_sum(:));
       
        imagesc(feature_maps_sum_count, [0 max_count]);
        colorbar
        title(sprintf('Fmaps histogram. Max count = %d.', max_count));
        fmaps_image_size = size(feature_maps_sum_count)
        saveas(gcf,sprintf('%splots_fmaps_histogram_%d.png', plots_dir, image_index));
        
        if(~show_binary_maps)
            figure();
            imagesc(feature_maps_sum_abs_coeff);
            colorbar
            title(sprintf('Fmaps sum of abs coeffs.'));
            saveas(gcf,sprintf('%splots_fmaps_sum_of_abs_coeffs_%d.png', plots_dir, image_index));
        end
    end
end