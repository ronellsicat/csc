function [] = write_tile_grid_image(image_in_name, tile_size, image_out_name)

    % http://stackoverflow.com/questions/4181913/in-matlab-how-to-draw-a-grid-over-an-image

    img = imread(image_in_name);  %# Load a sample 3-D RGB image
    img(tile_size(1):tile_size(1):end,:,:) = 0;       %# Change every tenth row to black
    img(:,tile_size(2):tile_size(2):end,:) = 0;       %# Change every tenth column to black
    
    imwrite(img, image_out_name);
end