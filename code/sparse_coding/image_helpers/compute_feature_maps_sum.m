function sum_image = compute_feature_maps_sum(feature_maps, show)

sum_image = sum(feature_maps, 3);

if(strcmp(show, 'yes') ~= 0)
   figure
   imagesc(sum_image);
   colorbar
end
end