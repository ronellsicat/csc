function out = arrange_image_names_by_number(names_list)

    out = names_list;

    for ii = 1: length(names_list)
       
        [pathstr,name,ext] = fileparts(names_list{1,ii});
        

           out{1,str2num(name)} = names_list{1,ii};
    end

end