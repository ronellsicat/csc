function [ linear_correlation ] = compute_correlation_on_mats( mat1, mat2, include_diagonal )
%COMPUTE_CORRELATION Computes linear correlation of two symmetric matrices.
%The computations disregard one triangle due to symmetry.

    % Just use the nonrepeating entries (lower triangle) of matrix:
    if(include_diagonal)
        
       nonrepeating_entries1 = mat1(tril(true(size(mat1))));
       nonrepeating_entries2 = mat2(tril(true(size(mat2))));
    else
        
       nonrepeating_entries1 = mat1(tril(true(size(mat1)), -1));
       nonrepeating_entries2 = mat2(tril(true(size(mat2)), -1));
    end
    
    linear_correlation = corrcoef(nonrepeating_entries1, nonrepeating_entries2);
    linear_correlation = linear_correlation(1,2);
end