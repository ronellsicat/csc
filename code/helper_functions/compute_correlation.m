function [ linear_correlation ] = compute_correlation( filename1, filename2, include_diagonal )
%COMPUTE_CORRELATION Computes linear correlation of two symmetric matrices, given
%their filenames on disk as mat files. The computations disregard one
%triangle due to symmetry.

    mat_struct1 = load(filename1);
    mat1 = get_first_field_value_from_struct(mat_struct1);
    
    mat_struct2 = load(filename2);
    mat2 = get_first_field_value_from_struct(mat_struct2);
    
    linear_correlation = compute_correlation_on_mats(mat1, mat2, include_diagonal);
end


function value = get_first_field_value_from_struct( input_struct ) 

    field_names = fieldnames(input_struct);
    value = getfield(input_struct, field_names{1,1});
end

