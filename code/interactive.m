function varargout = interactive(varargin)
% INTERACTIVE MATLAB code for interactive.fig
%      INTERACTIVE, by itself, creates a new INTERACTIVE or raises the existing
%      singleton*.
%
%      H = INTERACTIVE returns the handle to a new INTERACTIVE or the handle to
%      the existing singleton*.
%
%      INTERACTIVE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INTERACTIVE.M with the given input arguments.
%
%      INTERACTIVE('Property','Value',...) creates a new INTERACTIVE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before interactive_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to interactive_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help interactive

% Last Modified by GUIDE v2.5 17-Nov-2016 11:17:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @interactive_OpeningFcn, ...
                   'gui_OutputFcn',  @interactive_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before interactive is made visible.
function interactive_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to interactive (see VARARGIN)

% Choose default command line output for interactive
handles.output = hObject;

%% INITIALIZATIONS:
addpath('./external/subplot_tight');
addpath('./external/imtool3D');
addpath('./sparse_coding');
addpath('./external/histogram_distance');
addpath('./external/csh');
addpath('./external/MapN');
addpath('./external/covariancetoolbox-master');
addpath('./external/covariancetoolbox-master/lib/distance');
addpath('./external/image_similarity_toolbox-master');
installer
addpath('./helper_functions');

fprintf('WARNING: Needs previous call to code/external/csh/compile_mex.m.\n');

%  data if filename is provided as an argument:
if size(varargin,1) > 0
    data_filename = varargin{1}{1};
    handles = LoadCscData(handles, data_filename);
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes interactive wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = interactive_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on button press in create_button.
function create_button_Callback(hObject, eventdata, handles)
% hObject    handle to create_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch handles.popupmenu_analysis_type.Value;
case 1 
   handles = AnalysisCoeffHistogramBar(handles); 
case 2      
   handles = AnalysisCoeffHistogramMatrix(handles);
case 3
   handles = AnalysisFeatureMapsPerAtom(handles);
case 4
   handles = AnalysisFeatureMapsPerAtom(handles);
case 5
   handles = AnalysisHistogramSimilarity(handles);       
end

guidata(hObject,handles)

% --- Executes on button press in load_button.
function load_button_Callback(hObject, eventdata, handles)
% hObject    handle to load_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname] = uigetfile('*.mat', 'Select a MATLAB mat file');
handles = LoadCscData(handles, strcat(pathname, filename));

guidata(hObject,handles)


function handles = LoadCscData(handles, filename)

    handles.data = load(filename);
    
    % Rename variables if needed:
    if(isfield(handles.data ,'dictionary') == 0)
       handles.data.dictionary = handles.data.d;
       handles.data = rmfield(handles.data, 'd');
       handles.data.fmaps = handles.data.z;
       handles.data = rmfield(handles.data, 'z');
       handles.data.reconst = handles.data.Dz;
       handles.data = rmfield(handles.data, 'Dz');
    end
    
    [handles.data_dir, handles.data_filename] = fileparts(filename);
    handles = ComputeDataStats(handles);
    
    set(handles.text_k, 'string', size(handles.data.dictionary, 3));
    set(handles.text_num_images, 'string', size(handles.data.fmaps, 4));
    set(handles.text_max_abs_coeff, 'string', handles.max_abs_coeff);
    set(handles.edit_coeff_thr_max, 'string', handles.max_abs_coeff);
    
    %ShowDictionary(handles.dictionary);
    
    fprintf('Loaded csc data: %s.\n', filename);
    handles.data
    
    handles.plots_dir = strcat(handles.data_dir, '//plots/')
    if ~exist(handles.plots_dir, 'dir')
        mkdir(handles.plots_dir);
    end
    
function handles = ComputeDataStats(handles)
    
    coeff_max = 0;
    num_fmaps = size(handles.data.fmaps, 3);
    num_images = size(handles.data.fmaps, 4);
    for image_index = 1 : num_images
        for fmap_index = 1 : num_fmaps
       
            coeff_max = max( coeff_max, max(max(abs(handles.data.fmaps(:,:,fmap_index,image_index)))));
        end
    end
        
    handles.max_abs_coeff = coeff_max;
    fprintf('Max abs coeff is %f.\n', coeff_max);
    
function handles = AnalysisCoeffHistogramBar(handles)
        
    tile_size_x = str2double(handles.edit_tile_size.String);
    tile_size = [tile_size_x, tile_size_x];
    coeff_threshold_min = str2double(handles.edit_coeff_thr_min.String);
    coeff_threshold_max = str2double(handles.edit_coeff_thr_max.String);
    num_bins = str2double(handles.edit_num_coeff_bins.String);
    plot_max_y = str2double(handles.edit_plot_max_y.String);
    
    [handles.coeff_histograms, handles.image_tile_labels, handles.total_counted_coeffs_per_image] = vis_dictionary_histograms('bar', handles.data.dictionary, handles.data.fmaps, ...
        tile_size, coeff_threshold_min, coeff_threshold_max, num_bins, plot_max_y, handles.plots_dir);

    fprintf('Total counted coeffs per image is:\n');
    handles.total_counted_coeffs_per_image
    
function handles = AnalysisCoeffHistogramMatrix(handles)
        
    tile_size_x = str2double(handles.edit_tile_size.String);
    tile_size = [tile_size_x, tile_size_x];
    coeff_threshold_min = str2double(handles.edit_coeff_thr_min.String);
    coeff_threshold_max = str2double(handles.edit_coeff_thr_max.String);
    num_bins = str2double(handles.edit_num_coeff_bins.String);
    plot_max_y = str2double(handles.edit_plot_max_y.String);
    
    [handles.coeff_histograms, handles.image_tile_labels, handles.total_counted_coeffs_per_image] = vis_dictionary_histograms('matrix', ...
        handles.data.dictionary, handles.data.fmaps, ...
        tile_size, coeff_threshold_min, coeff_threshold_max, num_bins, plot_max_y, handles.plots_dir);
  
    fprintf('Total counted coeffs per image is:\n');
    handles.total_counted_coeffs_per_image
    
function handles = AnalysisFeatureMapsPerAtom(handles)
        
    tile_size_x = str2double(handles.edit_tile_size.String);
    tile_size = [tile_size_x, tile_size_x];
    coeff_threshold_min = str2double(handles.edit_coeff_thr_min.String);
    coeff_threshold_max = str2double(handles.edit_coeff_thr_max.String);
    show_binary_maps = 0;
    if (handles.popupmenu_analysis_type.Value == 4)
        show_binary_maps = 1;
    end
    
    vis_type = handles.popupmenu_vis_type.String{handles.popupmenu_vis_type.Value};
    
    sum_image = vis_fmaps_per_atom(handles.data.fmaps, tile_size, coeff_threshold_min, coeff_threshold_max, ...
        show_binary_maps, vis_type, handles.plots_dir);
    
    % Display orig image overlayed with coeff count sum:
    num_images = size(handles.data.orig_imgs, 3);

    atom_size = [size(handles.data.dictionary, 1), size(handles.data.dictionary,2)];
    image_boundary = [floor(atom_size(1)/2), floor(atom_size(2)/2)];
    
for image_index = 1 : num_images

%    show_img = imread(handles.data.image_names{image_index, 1});
 
    show_img = imread('../data/image_dbs/barbara512/1.png');
    sum_image = sum_image(image_boundary(1,1):end-image_boundary(1,1)-1, ...
    image_boundary(1,2):end-image_boundary(1,2)-1);

    figure();
    h = imshow(show_img, 'InitialMag', 'fit');
    set(h, 'AlphaData', (ones(size(sum_image)) .* 0.15) + (abs(sum_image) > 0) .* 0.85);
    title('Overlay of Coeff Counts on Input Image');
    saveas(gcf, sprintf('%soverlay_%d.png', handles.plots_dir, image_index))
end
    
function handles = ShowTiledInputs(handles)
    
    input_dirname = uigetdir('.', 'Select an input dir.')
   
    image_db_fnames = dir(input_dirname)
    
for ii = 3 : length(image_db_fnames) % Here we start with index 3 because 1 and 2 correspond to dirs: ., and ..

    tile_size_x = str2double(handles.edit_tile_size.String);
    tile_size = [tile_size_x, tile_size_x];
    img = imread(sprintf('%s/%s', image_db_fnames(ii).folder, image_db_fnames(ii).name));  %# Load a sample 3-D RGB image
    img(tile_size(1):tile_size(1):end,:,:) = 0;       %# Change every tenth row to black
    img(:,tile_size(2):tile_size(2):end,:) = 0; 
    figure();
    imshow(img);
end

guidata(hObject,handles)
        

function handles = AnalysisHistogramSimilarity(handles)

    if(isfield(handles, 'coeff_histograms'))
       
        handles.coeff_histograms_show = handles.coeff_histograms;
       
        if(handles.checkbox_hist_normalize.Value) 
            handles.coeff_histograms_show = handles.coeff_histograms;
            for rr = 1 : size(handles.coeff_histograms, 1)
               handles.coeff_histograms_show(rr,:) = handles.coeff_histograms(rr, :) ./ sum(handles.coeff_histograms(rr, :));
            end

            fprintf('Using normalized histograms.\n');
        end
        
        histogram_dist_type = handles.popupmenu_hist_dist_type.String{handles.popupmenu_hist_dist_type.Value};
        
        if(strcmp(histogram_dist_type, 'cityblock') || ...
            strcmp(histogram_dist_type, 'euclidean'))
            
            histogram_distances = pdist2(handles.coeff_histograms_show, handles.coeff_histograms_show, histogram_dist_type)            
        else
            % reference: https://www.mathworks.com/matlabcentral/fileexchange/39275-histogram-distances
            dist_func = str2func(histogram_dist_type);
             fprintf('Using %s histogram distance.\n', histogram_dist_type);
             
             if(strcmp(histogram_dist_type, 'histogram_mintersection'))
                 assert(handles.checkbox_hist_normalize.Value == 1);
             end
            histogram_distances = pdist2(handles.coeff_histograms_show, handles.coeff_histograms_show, dist_func)         
        end
 
        handles.histogram_distances = histogram_distances;
        figure();
        imagesc(histogram_distances);
        colormap gray;
        colorbar;
        set(gca, 'XAxisLocation', 'top');
        title(sprintf('Hist distances using %s metric. Normalized = %d.', ...
            histogram_dist_type, handles.checkbox_hist_normalize.Value));
        
        xticks([1:1:size(histogram_distances,2)]);
        xticklabels([1:1:size(histogram_distances,2)]);
        yticks([1:1:size(histogram_distances,1)]);
        yticklabels([1:1:size(histogram_distances,1)]);
        if(handles.checkbox_auto_save.Value)
            saveas(gcf, strcat(handles.plots_dir, 'dictionary_histogram_similarity_matrix.png'));
            
            colormap parula;
            saveas(gcf, strcat(handles.plots_dir, 'dictionary_histogram_similarity_matrix_parula.png'));
        end
        
        strcat(handles.plots_dir, 'dictionary_histogram_similarity.mat')
        save(strcat(handles.plots_dir, 'dictionary_histogram_similarity.mat'), 'histogram_distances');
        %save('dictionary_histogram_similarity.mat', histogram_distances);
        f = figure;
        t = uitable(f,'Data',histogram_distances);
        %t.ColumnName = tile_names;
        %set(t,'Position',[20 20 matrix_size*60 matrix_size*40]);
        writetable(table(histogram_distances), strcat(handles.plots_dir, 'dictionary_histogram_dists.csv'));
        drawnow;
        if(handles.checkbox_auto_save.Value)
            saveas(gcf, strcat(handles.plots_dir, 'dictionary_histogram_similarity_table.fig'));
        end
    end

% --- Executes on button press in pushbutton_close_all.
function pushbutton_close_all_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_close_all (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.figure1, 'HandleVisibility', 'off');
close all;
set(handles.figure1, 'HandleVisibility', 'on');


% --- Executes on button press in pushbutton_show_tiled_inputs.
function pushbutton_show_tiled_inputs_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_show_tiled_inputs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%handles = ShowTiledInputs(handles);

num_images = size(handles.data.orig_imgs, 3);

for image_index = 1 : num_images

    show_img = handles.data.orig_imgs(:,:, image_index);
    tile_size_x = str2double(handles.edit_tile_size.String);
    tile_size = [tile_size_x, tile_size_x]
    
    border_value = max(show_img(:));
    show_img(tile_size(1):tile_size(1):end,:,:) = border_value;
    show_img(:,tile_size(2):tile_size(2):end,:) = border_value; 
    figure();
    imagesc(show_img);
    colormap gray;
    colorbar;
end


% --- Executes on button press in pushbutton_show_dictionary.
function pushbutton_show_dictionary_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_show_dictionary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    %figure('units','normalized','outerposition',[0 0 1 1])
    figure();

    num_atoms = size(handles.data.fmaps, 3)
    num_atoms_in_col = ceil(sqrt(num_atoms));
    num_atoms_in_row = ceil(num_atoms / num_atoms_in_col);

   % title(sprintf('Dictionary with k = %d', num_atoms));
        
    for jj = 1:size(handles.data.dictionary,3)
        subplot_tight(num_atoms_in_row, num_atoms_in_col, jj, [0.01, 0.01]);
        imagesc(handles.data.dictionary(:,:, jj));
        colormap gray;
        axis image;
        axis off;
    end
   
    if(handles.checkbox_auto_save.Value)
        
        saveas(gcf, strcat(handles.plots_dir, 'dictionary.png'));
    end
    
% --- Executes on button press in pushbutton_show_reconstructions.
function pushbutton_show_reconstructions_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_show_reconstructions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

num_images = size(handles.data.reconst, 3);

for image_index = 1 : num_images
    figure();
    imagesc(handles.data.reconst(:,:, image_index));
    colormap gray;
    colorbar;
    title(sprintf('Reconst image %d.', image_index));
    if(handles.checkbox_auto_save.Value)
        saveas(gcf, strcat(handles.plots_dir, sprintf('reconst_%d.png',image_index)));
    end   
end


% --- Executes on button press in checkbox_hist_normalize.
function checkbox_hist_normalize_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_hist_normalize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_hist_normalize


% --- Executes on selection change in popupmenu_hist_dist_type.
function popupmenu_hist_dist_type_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_hist_dist_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_hist_dist_type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_hist_dist_type


% --- Executes during object creation, after setting all properties.
function popupmenu_hist_dist_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_hist_dist_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_show_orig.
function pushbutton_show_orig_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_show_orig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

num_images = size(handles.data.orig_imgs, 3);

for image_index = 1 : num_images
    figure();
    imagesc(handles.data.orig_imgs(:,:, image_index));
    colormap gray;
    colorbar;
    title(sprintf('Orig image %d.', image_index));
    if(handles.checkbox_auto_save.Value)
        saveas(gcf, strcat(handles.plots_dir, sprintf('orig_%d.png',image_index)));
    end
end


% --- Executes on button press in pushbutton_show_reconst_error.
function pushbutton_show_reconst_error_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_show_reconst_error (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

num_images = size(handles.data.orig_imgs, 3);
psnr_list = zeros(1, num_images);
rmse_list = zeros(1, num_images);

for image_index = 1 : num_images
    figure();
    boundary_size = floor(size(handles.data.dictionary, 1) / 2);
    reconst_no_boundary = handles.data.reconst(boundary_size+1:end-boundary_size,boundary_size+1:end-boundary_size, image_index);
    orig = handles.data.orig_imgs(:,:, image_index);
    imagesc(abs(orig - reconst_no_boundary));
    
    rmse_list(1, image_index) = sqrt(mean((orig(:) - reconst_no_boundary(:)).^2));
    psnr_list(1, image_index) = psnr(reconst_no_boundary, orig);
    
    colormap gray;
    colorbar;
    title(sprintf('Error image %d. RMSE = %f. PSNR = %f.', image_index, rmse_list(1, image_index), ...
    psnr_list(1, image_index)));

    if(handles.checkbox_auto_save.Value)
        saveas(gcf, strcat(handles.plots_dir, sprintf('reconst_error_%d.png',image_index)));
    end
end

% figure();
% bar(rmse_list);
% title('RMSE per image');
% figure();
% bar(psnr_list);
% title('PSNR per image');


% --- Executes during object creation, after setting all properties.
function popupmenu_vis_type_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_plot_max_y_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_num_coeff_bins_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_coeff_thr_max_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_coeff_thr_min_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_tile_size_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_tile_size_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function popupmenu_analysis_type_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function popupmenu_analysis_type_Callback(hObject, eventdata, handles)


% --- Executes on button press in pushbutton_compute_similarity_matrix.
function pushbutton_compute_similarity_matrix_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_compute_similarity_matrix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

test_only = true;

if(test_only)
    
    % Go through each similarity metric available and compute the matrix
    % and correlation with dictionary matrix.
    
    for ii = 1 : 8
       
        if(ii ~= 5 )
           continue; % For now only use method 5. 
        end
        
        fprintf(sprintf('\nAPPEARANCE METHOD %d.\n', ii));
        
        % Compute normalized_and_inverted_dissimilarity_matrix :
        method = ii;
        create_dendrogram = 0;
        compute_RDM = 1;
        visualize_2d = 1;
        n_img_categories = 0;
        out_dir = handles.plots_dir;
        img_names = handles.data.image_names;
        tile_size = str2num(handles.edit_similarity_tile_size.String)
        [F word_map RDM img_categories] = img_sim(out_dir, img_names, tile_size, method, create_dendrogram, compute_RDM, n_img_categories, visualize_2d);
        RDM
        RDM = ones(size(RDM)) - (RDM ./ max(RDM(:)))
        
        save(sprintf('%sappearance_sim_method_%d.mat', handles.plots_dir, method), 'RDM');
        
         % Compute correlation with dictionary histogram:
        correl = compute_correlation_on_mats(handles.histogram_distances, RDM, true);
        correl_no_diag = compute_correlation_on_mats(handles.histogram_distances, RDM, false);

        % Display RDM:
        figure();
        imagesc(RDM);
        title(sprintf('App Sim Mat %d, Corr_diag = %f, no_diag = %f', method, correl, correl_no_diag));
        colorbar
        colormap parula;
        caxis([0, 1]);
        xticks([1:1:size(RDM,2)]);
        xticklabels([1:1:size(RDM,2)]);
        yticks([1:1:size(RDM,1)]);
        yticklabels([1:1:size(RDM,1)]);
        saveas(gcf, sprintf('%sappearance_similarity_matrix_method_%d.png', handles.plots_dir, method));
       
    end
    
else

    if(length(handles.data.image_names) == 0) 
        return
    end

    handles.data.image_names
    patch_size = str2num(handles.edit_patch_size.String);
    num_hashtables = str2num(handles.edit_num_hashtables.String);
    image_codes = ComputeImageCodes(handles.data.image_names, patch_size, num_hashtables);

    tile_size = str2num(handles.edit_similarity_tile_size.String)
    handles.similarity_matrix = ComputeTileSimilarityMatrix(image_codes, tile_size, handles.plots_dir);
end

fprintf('\nDone computing tile similarity matrix.\n');
guidata(hObject,handles)

function edit_num_hashtables_Callback(hObject, eventdata, handles)
% hObject    handle to edit_num_hashtables (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_num_hashtables as text
%        str2double(get(hObject,'String')) returns contents of edit_num_hashtables as a double


% --- Executes during object creation, after setting all properties.
function edit_num_hashtables_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_num_hashtables (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_patch_size_Callback(hObject, eventdata, handles)
% hObject    handle to edit_patch_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_patch_size as text
%        str2double(get(hObject,'String')) returns contents of edit_patch_size as a double


% --- Executes during object creation, after setting all properties.
function edit_patch_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_patch_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_similarity_tile_size_Callback(hObject, eventdata, handles)
% hObject    handle to edit_similarity_tile_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_similarity_tile_size as text
%        str2double(get(hObject,'String')) returns contents of edit_similarity_tile_size as a double


% --- Executes during object creation, after setting all properties.
function edit_similarity_tile_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_similarity_tile_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_correlate.
function pushbutton_correlate_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_correlate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

fprintf('WARNING : Needs previous calls to COMPUTE SIMILARITY MATRIX and HISTOGRAM SIMILARITY MATRIX.\n');

% Just use the nonrepeating entries (lower triangle) of matrix:
nonrepeating_a = handles.similarity_matrix(tril(true(size(handles.similarity_matrix))));
nonrepeating_b = handles.histogram_distances(tril(true(size(handles.histogram_distances))));
nonrepeating_a_no_diag = handles.similarity_matrix(tril(true(size(handles.similarity_matrix)), -1));
nonrepeating_b_no_diag = handles.histogram_distances(tril(true(size(handles.histogram_distances)), -1));

correlation_coeffs = corrcoef(nonrepeating_a, nonrepeating_b);
correlation_coeffs_no_diag = corrcoef(nonrepeating_a_no_diag, nonrepeating_b_no_diag);
set(handles.text_correlation, 'String', num2str(correlation_coeffs(1,2)));
set(handles.text_correlation_no_diag, 'String', num2str(correlation_coeffs_no_diag(1,2)));

% Create a random matrix with a diagonal of 1 and measure correlation. This
% is just some sanity checker for the correlation value.
rand_tester = rand(size(handles.similarity_matrix));
for ii = 1 : size(rand_tester, 1)
   rand_tester(ii,ii) = 1.0; 
end

rand_tester = rand_tester(tril(true(size(handles.similarity_matrix))));

corr1 = corrcoef(nonrepeating_a, rand_tester)
corr2 = corrcoef(nonrepeating_b, rand_tester)

corr3 = corrcoef(nonrepeating_a, nonrepeating_a)
corr3 = corrcoef(nonrepeating_a_no_diag, nonrepeating_a_no_diag)

dist_euc_no_diag = pdist2(nonrepeating_a_no_diag', nonrepeating_b_no_diag', 'euclidean')
dist_cor_no_diag = pdist2(nonrepeating_a_no_diag', nonrepeating_b_no_diag', 'correlation')
dist_l1_no_diag = pdist2(nonrepeating_a_no_diag', nonrepeating_b_no_diag', 'cityblock')

dist_euc = pdist2(nonrepeating_a', nonrepeating_b', 'euclidean')
dist_cor = pdist2(nonrepeating_a', nonrepeating_b', 'correlation')
dist_l1 = pdist2(nonrepeating_a', nonrepeating_b', 'cityblock')

histogram_dist_type = handles.popupmenu_hist_dist_type.String{handles.popupmenu_hist_dist_type.Value};
dist_func = str2func(histogram_dist_type);
fprintf('Using %s histogram distance.\n', histogram_dist_type);
histogram_dist_no_diag = pdist2(nonrepeating_a_no_diag', nonrepeating_b_no_diag', dist_func) 
histogram_dist = pdist2(nonrepeating_a', nonrepeating_b', dist_func)
histogram_dist_same = pdist2(nonrepeating_a_no_diag', nonrepeating_a_no_diag', dist_func)

d_KL = distance_kullback(handles.similarity_matrix,handles.histogram_distances)
d_LogEuc = distance_logeuclid(handles.similarity_matrix,handles.histogram_distances)
d_rm = distance_riemann(handles.similarity_matrix,handles.histogram_distances)
d_opt = distance_opttransp(handles.similarity_matrix,handles.histogram_distances)
d_ld = distance_ld(handles.similarity_matrix,handles.histogram_distances)

function edit_plot_max_y_Callback(hObject, eventdata, handles)


% --- Executes on button press in checkbox_auto_save.
function checkbox_auto_save_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_auto_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_auto_save


% --- Executes on button press in pushbutton_save_matrix.
function pushbutton_save_matrix_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_save_matrix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname] = uiputfile('*.mat', 'MATLAB mat file');
fname = strcat(pathname, filename);
sim_matrix = handles.similarity_matrix;
save(fname, 'sim_matrix');

csvwrite(strcat(pathname, 'appearance_similarity_matrix_csh.csv'), sim_matrix);

% --- Executes on button press in pushbutton_load_matrix.
function pushbutton_load_matrix_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_load_matrix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname] = uigetfile('*.mat', 'Select a MATLAB mat file');
fname = strcat(pathname, filename)
tt = load(fname)
handles.similarity_matrix = tt.sim_matrix;

guidata(hObject,handles)


% --- Executes on button press in pushbutton_vis_appearance_sim_matrix.
function pushbutton_vis_appearance_sim_matrix_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_vis_appearance_sim_matrix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figure();
handles.similarity_matrix
imagesc(handles.similarity_matrix);
title('Appearance Similarity Matrix');
colorbar
colormap gray;
caxis([0, 1]);
xticks([1:1:size(handles.similarity_matrix,2)]);
xticklabels([1:1:size(handles.similarity_matrix,2)]);
yticks([1:1:size(handles.similarity_matrix,1)]);
yticklabels([1:1:size(handles.similarity_matrix,1)]);
saveas(gcf,sprintf('%splots_appearance_similarity_matrix.png', handles.plots_dir));

colormap parula;
saveas(gcf,sprintf('%splots_appearance_similarity_matrix_parula.png', handles.plots_dir));

