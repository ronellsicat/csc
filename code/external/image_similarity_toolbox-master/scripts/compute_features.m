if method == 1 %% pixel sse
	Fc = Ic;
elseif method == 2 % sift
	[junk Fc] = vl_sift(single(Ic)); % get keypoints for image
elseif method == 5 % gist
	imgr = imresize(Ic, [imageSize imageSize]);
    
    debug = true;
    
    % Computing gist requires 1) prefilter image, 2) filter image and collect
    % output energies
    if(debug)
        figure();
        imagesc(imgr);
        title('Resized image');
    end
    
    output = prefilt(double(imgr), 4);
    
    if(debug)
%         figure();
%         imagesc(output);
%         title('Prefiltered image');
%        output
    end
    
    Fc = gistGabor(output, numberBlocks, G);
    %normalize Fc
    Fc = Fc ./ sum(Fc);
elseif method == 4 % geoblur
	[I_blur1, Fc] = compute_points(Ic,find_points);
	
	I_blur(img) = { I_blur1 }; % blurred images
elseif method == 3 % jarrett model
	clear Fc
	run([full_path '/scripts/hier_model'])
elseif method == 6 % HMAX
	img_size = imageSize;
	Ic = imresize(Ic(50:350,50:350,:),[img_size img_size]);
	resp = s1_out(Ic,img_size);

	resp = resp - min(reshape(resp,[1 prod(size(resp))]));
	resp = resp./max(reshape(resp,[1 prod(size(resp))]));
	resp_c1(1,:,:,:,:,:) =  c1_out(resp(1:120,1:120,:,:,:)) ;
	resp_c1(2,:,:,:,:,:) =  c1_out(resp(1:120,30+[1:120],:,:,:)) ;
	resp_c1(3,:,:,:,:,:) =  c1_out(resp(1:120,60+[1:120],:,:,:)) ;
	resp_c1(4,:,:,:,:,:) =  c1_out(resp(30+[1:120],1:120,:,:,:)) ;
	resp_c1(5,:,:,:,:,:) =  c1_out(resp(30+[1:120],30+[1:120],:,:,:)) ;
	resp_c1(6,:,:,:,:,:) =  c1_out(resp(30+[1:120],60+[1:120],:,:,:)) ;
	resp_c1(7,:,:,:,:,:) =  c1_out(resp(60+[1:120],1:120,:,:,:)) ;
	resp_c1(8,:,:,:,:,:) =  c1_out(resp(60+[1:120],30+[1:120],:,:,:)) ;
	resp_c1(9,:,:,:,:,:) =  c1_out(resp(60+[1:120],60+[1:120],:,:,:)) ;
	Fc = reshape(resp_c1,[1 prod(size(resp_c1))]);
elseif method == 7 % shock graphs
	sm_cmd = [sm_cmd word_map{img} ' '];
elseif method == 8 % gabor filter bank
    fprintf(sprintf('tilesize is %d', imageSize));
        Ic=imresize(imSquare(Ic),[imageSize imageSize]);
        Fc=gaborProject(gBank,double(squeeze(sum(Ic,3))));
end
