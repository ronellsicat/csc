/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * TransitiveKNN_part2.c
 *
 * Code generation for function 'TransitiveKNN_part2'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "TransitiveKNN_part2.h"
#include "_coder_TransitiveKNN_part2_mex.h"

/* Type Definitions */
#ifndef struct_emxArray__common
#define struct_emxArray__common

struct emxArray__common
{
  void *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray__common*/

#ifndef typedef_emxArray__common
#define typedef_emxArray__common

typedef struct emxArray__common emxArray__common;

#endif                                 /*typedef_emxArray__common*/

#ifndef struct_emxArray_boolean_T
#define struct_emxArray_boolean_T

struct emxArray_boolean_T
{
  boolean_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_boolean_T*/

#ifndef typedef_emxArray_boolean_T
#define typedef_emxArray_boolean_T

typedef struct emxArray_boolean_T emxArray_boolean_T;

#endif                                 /*typedef_emxArray_boolean_T*/

#ifndef struct_emxArray_int32_T
#define struct_emxArray_int32_T

struct emxArray_int32_T
{
  int32_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_int32_T*/

#ifndef typedef_emxArray_int32_T
#define typedef_emxArray_int32_T

typedef struct emxArray_int32_T emxArray_int32_T;

#endif                                 /*typedef_emxArray_int32_T*/

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131435U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "TransitiveKNN_part2",               /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

static emlrtRSInfo emlrtRSI = { 14,    /* lineNo */
  "TransitiveKNN_part2",               /* fcnName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 15,  /* lineNo */
  "TransitiveKNN_part2",               /* fcnName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 17,  /* lineNo */
  "TransitiveKNN_part2",               /* fcnName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 22,  /* lineNo */
  "TransitiveKNN_part2",               /* fcnName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 28,  /* lineNo */
  "TransitiveKNN_part2",               /* fcnName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 34,  /* lineNo */
  "TransitiveKNN_part2",               /* fcnName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 28,  /* lineNo */
  "squeeze",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\squeeze.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 58,  /* lineNo */
  "squeeze",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\squeeze.m"/* pathName */
};

static emlrtRSInfo i_emlrtRSI = { 20,  /* lineNo */
  "eml_int_forloop_overflow_check",    /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\eml\\eml_int_forloop_overflow_check.m"/* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 26,  /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\datafun\\sort.m"/* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 48,  /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 69,  /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 72,  /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRSInfo n_emlrtRSI = { 74,  /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRSInfo o_emlrtRSI = { 77,  /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRSInfo p_emlrtRSI = { 80,  /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

static emlrtRSInfo q_emlrtRSI = { 82,  /* lineNo */
  "sort",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pathName */
};

emlrtRSInfo r_emlrtRSI = { 49,         /* lineNo */
  "prodsize",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\prodsize.m"/* pathName */
};

static emlrtRSInfo s_emlrtRSI = { 70,  /* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo t_emlrtRSI = { 247, /* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo u_emlrtRSI = { 255, /* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo v_emlrtRSI = { 256, /* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo w_emlrtRSI = { 264, /* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo x_emlrtRSI = { 272, /* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo y_emlrtRSI = { 329, /* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo ab_emlrtRSI = { 357,/* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo bb_emlrtRSI = { 364,/* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo cb_emlrtRSI = { 524,/* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo db_emlrtRSI = { 526,/* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo eb_emlrtRSI = { 554,/* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo fb_emlrtRSI = { 436,/* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

emlrtRSInfo gb_emlrtRSI = { 443,       /* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo hb_emlrtRSI = { 444,/* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo ib_emlrtRSI = { 451,/* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo jb_emlrtRSI = { 498,/* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo kb_emlrtRSI = { 467,/* lineNo */
  "sortIdx",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pathName */
};

static emlrtRSInfo lb_emlrtRSI = { 108,/* lineNo */
  "diff",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\datafun\\diff.m"/* pathName */
};

static emlrtRSInfo mb_emlrtRSI = { 106,/* lineNo */
  "diff",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\datafun\\diff.m"/* pathName */
};

static emlrtRSInfo nb_emlrtRSI = { 44, /* lineNo */
  "find",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo ob_emlrtRSI = { 234,/* lineNo */
  "find",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo pb_emlrtRSI = { 253,/* lineNo */
  "find",                              /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pathName */
};

static emlrtRSInfo qb_emlrtRSI = { 18, /* lineNo */
  "indexShapeCheck",                   /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\indexShapeCheck.m"/* pathName */
};

static emlrtRSInfo rb_emlrtRSI = { 19, /* lineNo */
  "ind2sub",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\ind2sub.m"/* pathName */
};

static emlrtRSInfo sb_emlrtRSI = { 55, /* lineNo */
  "permute",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\permute.m"/* pathName */
};

static emlrtRSInfo tb_emlrtRSI = { 75, /* lineNo */
  "permute",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\permute.m"/* pathName */
};

static emlrtRTEInfo emlrtRTEI = { 1,   /* lineNo */
  60,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pName */
};

static emlrtRTEInfo b_emlrtRTEI = { 253,/* lineNo */
  13,                                  /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo c_emlrtRTEI = { 15,/* lineNo */
  9,                                   /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pName */
};

static emlrtRTEInfo d_emlrtRTEI = { 16,/* lineNo */
  9,                                   /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pName */
};

static emlrtRTEInfo e_emlrtRTEI = { 17,/* lineNo */
  9,                                   /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pName */
};

static emlrtRTEInfo f_emlrtRTEI = { 19,/* lineNo */
  9,                                   /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pName */
};

static emlrtRTEInfo g_emlrtRTEI = { 1, /* lineNo */
  11,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pName */
};

static emlrtRTEInfo h_emlrtRTEI = { 36,/* lineNo */
  6,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo i_emlrtRTEI = { 42,/* lineNo */
  5,                                   /* colNo */
  "ind2sub",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\ind2sub.m"/* pName */
};

static emlrtRTEInfo j_emlrtRTEI = { 1, /* lineNo */
  14,                                  /* colNo */
  "squeeze",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\squeeze.m"/* pName */
};

static emlrtRTEInfo k_emlrtRTEI = { 1, /* lineNo */
  20,                                  /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pName */
};

static emlrtRTEInfo l_emlrtRTEI = { 1, /* lineNo */
  20,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo m_emlrtRTEI = { 1, /* lineNo */
  14,                                  /* colNo */
  "diff",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\datafun\\diff.m"/* pName */
};

static emlrtRTEInfo n_emlrtRTEI = { 1, /* lineNo */
  1,                                   /* colNo */
  "_coder_TransitiveKNN_part2_api",    /* fName */
  ""                                   /* pName */
};

static emlrtRTEInfo o_emlrtRTEI = { 50,/* lineNo */
  1,                                   /* colNo */
  "sort",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sort.m"/* pName */
};

static emlrtRTEInfo p_emlrtRTEI = { 322,/* lineNo */
  9,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo q_emlrtRTEI = { 324,/* lineNo */
  9,                                   /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo r_emlrtRTEI = { 247,/* lineNo */
  14,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo s_emlrtRTEI = { 247,/* lineNo */
  20,                                  /* colNo */
  "sortIdx",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\sortIdx.m"/* pName */
};

static emlrtRTEInfo t_emlrtRTEI = { 12,/* lineNo */
  9,                                   /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pName */
};

static emlrtRTEInfo u_emlrtRTEI = { 13,/* lineNo */
  13,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pName */
};

static emlrtBCInfo emlrtBCI = { -1,    /* iFirst */
  -1,                                  /* iLast */
  14,                                  /* lineNo */
  49,                                  /* colNo */
  "errorMat",                          /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo b_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  14,                                  /* lineNo */
  51,                                  /* colNo */
  "errorMat",                          /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo c_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  15,                                  /* lineNo */
  39,                                  /* colNo */
  "overSafeResults",                   /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo d_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  15,                                  /* lineNo */
  41,                                  /* colNo */
  "overSafeResults",                   /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo e_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  22,                                  /* lineNo */
  29,                                  /* colNo */
  "uOSRinds",                          /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo f_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  24,                                  /* lineNo */
  13,                                  /* colNo */
  "uOSRFinalinds",                     /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo emlrtECI = { -1,    /* nDims */
  24,                                  /* lineNo */
  13,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pName */
};

static emlrtBCInfo g_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  25,                                  /* lineNo */
  13,                                  /* colNo */
  "uOSRFinalinds",                     /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo h_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  28,                                  /* lineNo */
  20,                                  /* colNo */
  "CSH_ann",                           /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo i_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  28,                                  /* lineNo */
  22,                                  /* colNo */
  "CSH_ann",                           /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo b_emlrtECI = { -1,  /* nDims */
  28,                                  /* lineNo */
  10,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pName */
};

static emlrtBCInfo j_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  28,                                  /* lineNo */
  37,                                  /* colNo */
  "CSH_ann",                           /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo k_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  28,                                  /* lineNo */
  39,                                  /* colNo */
  "CSH_ann",                           /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo c_emlrtECI = { -1,  /* nDims */
  28,                                  /* lineNo */
  27,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pName */
};

static emlrtBCInfo l_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  30,                                  /* lineNo */
  22,                                  /* colNo */
  "sortedErrors",                      /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo m_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  30,                                  /* lineNo */
  24,                                  /* colNo */
  "sortedErrors",                      /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo d_emlrtECI = { -1,  /* nDims */
  30,                                  /* lineNo */
  9,                                   /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m"/* pName */
};

static emlrtRTEInfo v_emlrtRTEI = { 243,/* lineNo */
  9,                                   /* colNo */
  "find",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\find.m"/* pName */
};

static emlrtRTEInfo w_emlrtRTEI = { 38,/* lineNo */
  15,                                  /* colNo */
  "ind2sub",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\elmat\\ind2sub.m"/* pName */
};

static emlrtDCInfo emlrtDCI = { 8,     /* lineNo */
  34,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo b_emlrtDCI = { 8,   /* lineNo */
  34,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo c_emlrtDCI = { 8,   /* lineNo */
  59,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo d_emlrtDCI = { 8,   /* lineNo */
  59,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo e_emlrtDCI = { 9,   /* lineNo */
  17,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtDCInfo f_emlrtDCI = { 9,   /* lineNo */
  17,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  4                                    /* checkKind */
};

static emlrtDCInfo g_emlrtDCI = { 10,  /* lineNo */
  28,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo n_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  16,                                  /* lineNo */
  16,                                  /* colNo */
  "OSR",                               /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtDCInfo h_emlrtDCI = { 19,  /* lineNo */
  31,                                  /* colNo */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo o_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  20,                                  /* lineNo */
  34,                                  /* colNo */
  "numUniqueResultsPerPixel",          /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo p_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  20,                                  /* lineNo */
  36,                                  /* colNo */
  "numUniqueResultsPerPixel",          /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo q_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  25,                                  /* lineNo */
  42,                                  /* colNo */
  "uOSRinds",                          /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo r_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  28,                                  /* lineNo */
  63,                                  /* colNo */
  "sOSR",                              /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo s_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  30,                                  /* lineNo */
  31,                                  /* colNo */
  "sErrors",                           /* aName */
  "TransitiveKNN_part2",               /* fName */
  "C:\\Users\\rsicat\\Bitbucket\\ronellsicat_csc\\code\\external\\csh\\C_and_Mex\\TransitiveKNN_part2.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRTEInfo x_emlrtRTEI = { 86,/* lineNo */
  15,                                  /* colNo */
  "eml_int_forloop_overflow_check",    /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\eml\\eml_int_forloop_overflow_check.m"/* pName */
};

static emlrtRTEInfo y_emlrtRTEI = { 51,/* lineNo */
  19,                                  /* colNo */
  "diff",                              /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\lib\\matlab\\datafun\\diff.m"/* pName */
};

static emlrtRTEInfo ab_emlrtRTEI = { 88,/* lineNo */
  9,                                   /* colNo */
  "indexShapeCheck",                   /* fName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\indexShapeCheck.m"/* pName */
};

static emlrtRSInfo ub_emlrtRSI = { 18, /* lineNo */
  "indexDivide",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2016b\\toolbox\\eml\\eml\\+coder\\+internal\\indexDivide.m"/* pathName */
};

/* Function Declarations */
static void b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_uint8_T *y);
static const mxArray *b_emlrt_marshallOut(const emxArray_real_T *u);
static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *k, const
  char_T *identifier);
static const mxArray *c_emlrt_marshallOut(const emxArray_real_T *u);
static void check_forloop_overflow_error(const emlrtStack *sp);
static real_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static void diff(const emlrtStack *sp, const emxArray_real_T *x, emxArray_real_T
                 *y);
static int32_T div_s32(const emlrtStack *sp, int32_T numerator, int32_T
  denominator);
static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *overSafeResults, const char_T *identifier, emxArray_real_T *y);
static void emlrt_marshallIn(const emlrtStack *sp, const mxArray *A, const
  char_T *identifier, emxArray_uint8_T *y);
static const mxArray *emlrt_marshallOut(const emxArray_real_T *u);
static void emxEnsureCapacity(const emlrtStack *sp, emxArray__common *emxArray,
  int32_T oldNumel, int32_T elementSize, const emlrtRTEInfo *srcLocation);
static void emxFree_boolean_T(emxArray_boolean_T **pEmxArray);
static void emxFree_int32_T(emxArray_int32_T **pEmxArray);
static void emxFree_real_T(emxArray_real_T **pEmxArray);
static void emxFree_uint8_T(emxArray_uint8_T **pEmxArray);
static void emxInit_boolean_T(const emlrtStack *sp, emxArray_boolean_T
  **pEmxArray, int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T
  doPush);
static void emxInit_int32_T(const emlrtStack *sp, emxArray_int32_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush);
static void emxInit_int32_T1(const emlrtStack *sp, emxArray_int32_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush);
static void emxInit_int32_T2(const emlrtStack *sp, emxArray_int32_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush);
static void emxInit_real_T(const emlrtStack *sp, emxArray_real_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush);
static void emxInit_real_T1(const emlrtStack *sp, emxArray_real_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush);
static void emxInit_real_T2(const emlrtStack *sp, emxArray_real_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush);
static void emxInit_real_T3(const emlrtStack *sp, emxArray_real_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush);
static void emxInit_uint8_T(const emlrtStack *sp, emxArray_uint8_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush);
static void f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_uint8_T *ret);
static real_T h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId);
static void i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static void indexShapeCheck(const emlrtStack *sp, int32_T matrixSize, const
  int32_T indexSize[2]);
static void merge(const emlrtStack *sp, emxArray_int32_T *idx, emxArray_real_T
                  *x, int32_T offset, int32_T np, int32_T nq, emxArray_int32_T
                  *iwork, emxArray_real_T *xwork);
static void merge_block(const emlrtStack *sp, emxArray_int32_T *idx,
  emxArray_real_T *x, int32_T offset, int32_T n, int32_T preSortLevel,
  emxArray_int32_T *iwork, emxArray_real_T *xwork);
static void sort(const emlrtStack *sp, emxArray_real_T *x, emxArray_int32_T *idx);
static void sortIdx(const emlrtStack *sp, emxArray_real_T *x, emxArray_int32_T
                    *idx);
static void squeeze(const emlrtStack *sp, const emxArray_real_T *a,
                    emxArray_real_T *b);

/* Function Definitions */
static void b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_uint8_T *y)
{
  g_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static const mxArray *b_emlrt_marshallOut(const emxArray_real_T *u)
{
  const mxArray *y;
  const mxArray *m1;
  static const int32_T iv6[2] = { 0, 0 };

  y = NULL;
  m1 = emlrtCreateNumericArray(2, iv6, mxDOUBLE_CLASS, mxREAL);
  mxSetData((mxArray *)m1, (void *)&u->data[0]);
  emlrtSetDimensions((mxArray *)m1, u->size, 2);
  emlrtAssign(&y, m1);
  return y;
}

static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *k, const
  char_T *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = d_emlrt_marshallIn(sp, emlrtAlias(k), &thisId);
  emlrtDestroyArray(&k);
  return y;
}

static const mxArray *c_emlrt_marshallOut(const emxArray_real_T *u)
{
  const mxArray *y;
  const mxArray *m2;
  static const int32_T iv7[3] = { 0, 0, 0 };

  y = NULL;
  m2 = emlrtCreateNumericArray(3, iv7, mxDOUBLE_CLASS, mxREAL);
  mxSetData((mxArray *)m2, (void *)&u->data[0]);
  emlrtSetDimensions((mxArray *)m2, u->size, 3);
  emlrtAssign(&y, m2);
  return y;
}

/*
 *
 */
static void check_forloop_overflow_error(const emlrtStack *sp)
{
  emlrtErrorWithMessageIdR2012b(sp, &x_emlrtRTEI,
    "Coder:toolbox:int_forloop_overflow", 3, 4, 5, "int32");
}

static real_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = h_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

/*
 *
 */
static void diff(const emlrtStack *sp, const emxArray_real_T *x, emxArray_real_T
                 *y)
{
  int32_T ySize_idx_0;
  int32_T iyLead;
  boolean_T overflow;
  real_T work_data_idx_0;
  int32_T m;
  real_T tmp1;
  real_T tmp2;
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  if (x->size[0] == 0) {
    iyLead = y->size[0];
    y->size[0] = 0;
    emxEnsureCapacity(sp, (emxArray__common *)y, iyLead, (int32_T)sizeof(real_T),
                      &m_emlrtRTEI);
  } else {
    ySize_idx_0 = x->size[0] - 1;
    if (muIntScalarMin_sint32(ySize_idx_0, 1) < 1) {
      iyLead = y->size[0];
      y->size[0] = 0;
      emxEnsureCapacity(sp, (emxArray__common *)y, iyLead, (int32_T)sizeof
                        (real_T), &m_emlrtRTEI);
    } else {
      overflow = (x->size[0] != 1);
      if (!overflow) {
        emlrtErrorWithMessageIdR2012b(sp, &y_emlrtRTEI,
          "Coder:toolbox:autoDimIncompatibility", 0);
      }

      ySize_idx_0 = x->size[0] - 1;
      iyLead = y->size[0];
      y->size[0] = ySize_idx_0;
      emxEnsureCapacity(sp, (emxArray__common *)y, iyLead, (int32_T)sizeof
                        (real_T), &m_emlrtRTEI);
      if (!(y->size[0] == 0)) {
        ySize_idx_0 = 1;
        iyLead = 0;
        work_data_idx_0 = x->data[0];
        st.site = &mb_emlrtRSI;
        overflow = ((!(2 > x->size[0])) && (x->size[0] > 2147483646));
        if (overflow) {
          b_st.site = &i_emlrtRSI;
          check_forloop_overflow_error(&b_st);
        }

        for (m = 2; m <= x->size[0]; m++) {
          tmp1 = x->data[ySize_idx_0];
          st.site = &lb_emlrtRSI;
          tmp2 = work_data_idx_0;
          work_data_idx_0 = tmp1;
          tmp1 -= tmp2;
          ySize_idx_0++;
          y->data[iyLead] = tmp1;
          iyLead++;
        }
      }
    }
  }
}

static int32_T div_s32(const emlrtStack *sp, int32_T numerator, int32_T
  denominator)
{
  int32_T quotient;
  uint32_T absNumerator;
  uint32_T absDenominator;
  boolean_T quotientNeedsNegation;
  if (denominator == 0) {
    if (numerator >= 0) {
      quotient = MAX_int32_T;
    } else {
      quotient = MIN_int32_T;
    }

    emlrtDivisionByZeroErrorR2012b(NULL, sp);
  } else {
    if (numerator < 0) {
      absNumerator = ~(uint32_T)numerator + 1U;
    } else {
      absNumerator = (uint32_T)numerator;
    }

    if (denominator < 0) {
      absDenominator = ~(uint32_T)denominator + 1U;
    } else {
      absDenominator = (uint32_T)denominator;
    }

    quotientNeedsNegation = ((numerator < 0) != (denominator < 0));
    absNumerator /= absDenominator;
    if (quotientNeedsNegation) {
      quotient = -(int32_T)absNumerator;
    } else {
      quotient = (int32_T)absNumerator;
    }
  }

  return quotient;
}

static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *overSafeResults, const char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  f_emlrt_marshallIn(sp, emlrtAlias(overSafeResults), &thisId, y);
  emlrtDestroyArray(&overSafeResults);
}

static void emlrt_marshallIn(const emlrtStack *sp, const mxArray *A, const
  char_T *identifier, emxArray_uint8_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  b_emlrt_marshallIn(sp, emlrtAlias(A), &thisId, y);
  emlrtDestroyArray(&A);
}

static const mxArray *emlrt_marshallOut(const emxArray_real_T *u)
{
  const mxArray *y;
  const mxArray *m0;
  static const int32_T iv5[4] = { 0, 0, 0, 0 };

  y = NULL;
  m0 = emlrtCreateNumericArray(4, iv5, mxDOUBLE_CLASS, mxREAL);
  mxSetData((mxArray *)m0, (void *)&u->data[0]);
  emlrtSetDimensions((mxArray *)m0, u->size, 4);
  emlrtAssign(&y, m0);
  return y;
}

static void emxEnsureCapacity(const emlrtStack *sp, emxArray__common *emxArray,
  int32_T oldNumel, int32_T elementSize, const emlrtRTEInfo *srcLocation)
{
  int32_T newNumel;
  int32_T i;
  void *newData;
  if (oldNumel < 0) {
    oldNumel = 0;
  }

  newNumel = 1;
  for (i = 0; i < emxArray->numDimensions; i++) {
    newNumel = (int32_T)emlrtSizeMulR2012b((uint32_T)newNumel, (uint32_T)
      emxArray->size[i], srcLocation, sp);
  }

  if (newNumel > emxArray->allocatedSize) {
    i = emxArray->allocatedSize;
    if (i < 16) {
      i = 16;
    }

    while (i < newNumel) {
      if (i > 1073741823) {
        i = MAX_int32_T;
      } else {
        i <<= 1;
      }
    }

    newData = emlrtCallocMex((uint32_T)i, (uint32_T)elementSize);
    if (newData == NULL) {
      emlrtHeapAllocationErrorR2012b(srcLocation, sp);
    }

    if (emxArray->data != NULL) {
      memcpy(newData, emxArray->data, (uint32_T)(elementSize * oldNumel));
      if (emxArray->canFreeData) {
        emlrtFreeMex(emxArray->data);
      }
    }

    emxArray->data = newData;
    emxArray->allocatedSize = i;
    emxArray->canFreeData = true;
  }
}

static void emxFree_boolean_T(emxArray_boolean_T **pEmxArray)
{
  if (*pEmxArray != (emxArray_boolean_T *)NULL) {
    if (((*pEmxArray)->data != (boolean_T *)NULL) && (*pEmxArray)->canFreeData)
    {
      emlrtFreeMex((void *)(*pEmxArray)->data);
    }

    emlrtFreeMex((void *)(*pEmxArray)->size);
    emlrtFreeMex((void *)*pEmxArray);
    *pEmxArray = (emxArray_boolean_T *)NULL;
  }
}

static void emxFree_int32_T(emxArray_int32_T **pEmxArray)
{
  if (*pEmxArray != (emxArray_int32_T *)NULL) {
    if (((*pEmxArray)->data != (int32_T *)NULL) && (*pEmxArray)->canFreeData) {
      emlrtFreeMex((void *)(*pEmxArray)->data);
    }

    emlrtFreeMex((void *)(*pEmxArray)->size);
    emlrtFreeMex((void *)*pEmxArray);
    *pEmxArray = (emxArray_int32_T *)NULL;
  }
}

static void emxFree_real_T(emxArray_real_T **pEmxArray)
{
  if (*pEmxArray != (emxArray_real_T *)NULL) {
    if (((*pEmxArray)->data != (real_T *)NULL) && (*pEmxArray)->canFreeData) {
      emlrtFreeMex((void *)(*pEmxArray)->data);
    }

    emlrtFreeMex((void *)(*pEmxArray)->size);
    emlrtFreeMex((void *)*pEmxArray);
    *pEmxArray = (emxArray_real_T *)NULL;
  }
}

static void emxFree_uint8_T(emxArray_uint8_T **pEmxArray)
{
  if (*pEmxArray != (emxArray_uint8_T *)NULL) {
    if (((*pEmxArray)->data != (uint8_T *)NULL) && (*pEmxArray)->canFreeData) {
      emlrtFreeMex((void *)(*pEmxArray)->data);
    }

    emlrtFreeMex((void *)(*pEmxArray)->size);
    emlrtFreeMex((void *)*pEmxArray);
    *pEmxArray = (emxArray_uint8_T *)NULL;
  }
}

static void emxInit_boolean_T(const emlrtStack *sp, emxArray_boolean_T
  **pEmxArray, int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T
  doPush)
{
  emxArray_boolean_T *emxArray;
  int32_T i;
  *pEmxArray = (emxArray_boolean_T *)emlrtMallocMex(sizeof(emxArray_boolean_T));
  if ((void *)*pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  if (doPush) {
    emlrtPushHeapReferenceStackR2012b(sp, (void *)pEmxArray, (void (*)(void *))
      emxFree_boolean_T);
  }

  emxArray = *pEmxArray;
  emxArray->data = (boolean_T *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int32_T *)emlrtMallocMex((uint32_T)(sizeof(int32_T)
    * numDimensions));
  if ((void *)emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

static void emxInit_int32_T(const emlrtStack *sp, emxArray_int32_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush)
{
  emxArray_int32_T *emxArray;
  int32_T i;
  *pEmxArray = (emxArray_int32_T *)emlrtMallocMex(sizeof(emxArray_int32_T));
  if ((void *)*pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  if (doPush) {
    emlrtPushHeapReferenceStackR2012b(sp, (void *)pEmxArray, (void (*)(void *))
      emxFree_int32_T);
  }

  emxArray = *pEmxArray;
  emxArray->data = (int32_T *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int32_T *)emlrtMallocMex((uint32_T)(sizeof(int32_T)
    * numDimensions));
  if ((void *)emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

static void emxInit_int32_T1(const emlrtStack *sp, emxArray_int32_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush)
{
  emxArray_int32_T *emxArray;
  int32_T i;
  *pEmxArray = (emxArray_int32_T *)emlrtMallocMex(sizeof(emxArray_int32_T));
  if ((void *)*pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  if (doPush) {
    emlrtPushHeapReferenceStackR2012b(sp, (void *)pEmxArray, (void (*)(void *))
      emxFree_int32_T);
  }

  emxArray = *pEmxArray;
  emxArray->data = (int32_T *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int32_T *)emlrtMallocMex((uint32_T)(sizeof(int32_T)
    * numDimensions));
  if ((void *)emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

static void emxInit_int32_T2(const emlrtStack *sp, emxArray_int32_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush)
{
  emxArray_int32_T *emxArray;
  int32_T i;
  *pEmxArray = (emxArray_int32_T *)emlrtMallocMex(sizeof(emxArray_int32_T));
  if ((void *)*pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  if (doPush) {
    emlrtPushHeapReferenceStackR2012b(sp, (void *)pEmxArray, (void (*)(void *))
      emxFree_int32_T);
  }

  emxArray = *pEmxArray;
  emxArray->data = (int32_T *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int32_T *)emlrtMallocMex((uint32_T)(sizeof(int32_T)
    * numDimensions));
  if ((void *)emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

static void emxInit_real_T(const emlrtStack *sp, emxArray_real_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush)
{
  emxArray_real_T *emxArray;
  int32_T i;
  *pEmxArray = (emxArray_real_T *)emlrtMallocMex(sizeof(emxArray_real_T));
  if ((void *)*pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  if (doPush) {
    emlrtPushHeapReferenceStackR2012b(sp, (void *)pEmxArray, (void (*)(void *))
      emxFree_real_T);
  }

  emxArray = *pEmxArray;
  emxArray->data = (real_T *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int32_T *)emlrtMallocMex((uint32_T)(sizeof(int32_T)
    * numDimensions));
  if ((void *)emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

static void emxInit_real_T1(const emlrtStack *sp, emxArray_real_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush)
{
  emxArray_real_T *emxArray;
  int32_T i;
  *pEmxArray = (emxArray_real_T *)emlrtMallocMex(sizeof(emxArray_real_T));
  if ((void *)*pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  if (doPush) {
    emlrtPushHeapReferenceStackR2012b(sp, (void *)pEmxArray, (void (*)(void *))
      emxFree_real_T);
  }

  emxArray = *pEmxArray;
  emxArray->data = (real_T *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int32_T *)emlrtMallocMex((uint32_T)(sizeof(int32_T)
    * numDimensions));
  if ((void *)emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

static void emxInit_real_T2(const emlrtStack *sp, emxArray_real_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush)
{
  emxArray_real_T *emxArray;
  int32_T i;
  *pEmxArray = (emxArray_real_T *)emlrtMallocMex(sizeof(emxArray_real_T));
  if ((void *)*pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  if (doPush) {
    emlrtPushHeapReferenceStackR2012b(sp, (void *)pEmxArray, (void (*)(void *))
      emxFree_real_T);
  }

  emxArray = *pEmxArray;
  emxArray->data = (real_T *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int32_T *)emlrtMallocMex((uint32_T)(sizeof(int32_T)
    * numDimensions));
  if ((void *)emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

static void emxInit_real_T3(const emlrtStack *sp, emxArray_real_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush)
{
  emxArray_real_T *emxArray;
  int32_T i;
  *pEmxArray = (emxArray_real_T *)emlrtMallocMex(sizeof(emxArray_real_T));
  if ((void *)*pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  if (doPush) {
    emlrtPushHeapReferenceStackR2012b(sp, (void *)pEmxArray, (void (*)(void *))
      emxFree_real_T);
  }

  emxArray = *pEmxArray;
  emxArray->data = (real_T *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int32_T *)emlrtMallocMex((uint32_T)(sizeof(int32_T)
    * numDimensions));
  if ((void *)emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

static void emxInit_uint8_T(const emlrtStack *sp, emxArray_uint8_T **pEmxArray,
  int32_T numDimensions, const emlrtRTEInfo *srcLocation, boolean_T doPush)
{
  emxArray_uint8_T *emxArray;
  int32_T i;
  *pEmxArray = (emxArray_uint8_T *)emlrtMallocMex(sizeof(emxArray_uint8_T));
  if ((void *)*pEmxArray == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  if (doPush) {
    emlrtPushHeapReferenceStackR2012b(sp, (void *)pEmxArray, (void (*)(void *))
      emxFree_uint8_T);
  }

  emxArray = *pEmxArray;
  emxArray->data = (uint8_T *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int32_T *)emlrtMallocMex((uint32_T)(sizeof(int32_T)
    * numDimensions));
  if ((void *)emxArray->size == NULL) {
    emlrtHeapAllocationErrorR2012b(srcLocation, sp);
  }

  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

static void f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  i_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_uint8_T *ret)
{
  static const int32_T dims[3] = { -1, -1, 3 };

  boolean_T bv0[3] = { true, true, true };

  int32_T iv8[3];
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "uint8", false, 3U, dims, &bv0[0],
    iv8);
  ret->size[0] = iv8[0];
  ret->size[1] = iv8[1];
  ret->size[2] = iv8[2];
  ret->allocatedSize = ret->size[0] * ret->size[1] * ret->size[2];
  ret->data = (uint8_T *)mxGetData(src);
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

static real_T h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static void i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[3] = { -1, -1, -1 };

  boolean_T bv1[3] = { true, true, true };

  int32_T iv9[3];
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 3U, dims, &bv1[0],
    iv9);
  ret->size[0] = iv9[0];
  ret->size[1] = iv9[1];
  ret->size[2] = iv9[2];
  ret->allocatedSize = ret->size[0] * ret->size[1] * ret->size[2];
  ret->data = (real_T *)mxGetData(src);
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

/*
 *
 */
static void indexShapeCheck(const emlrtStack *sp, int32_T matrixSize, const
  int32_T indexSize[2])
{
  boolean_T guard1 = false;
  boolean_T nonSingletonDimFound;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  guard1 = false;
  if (!(matrixSize != 1)) {
    nonSingletonDimFound = false;
    if (indexSize[1] != 1) {
      nonSingletonDimFound = true;
    }

    if (nonSingletonDimFound) {
      nonSingletonDimFound = true;
    } else {
      guard1 = true;
    }
  } else {
    guard1 = true;
  }

  if (guard1) {
    nonSingletonDimFound = false;
  }

  st.site = &qb_emlrtRSI;
  if (nonSingletonDimFound) {
    emlrtErrorWithMessageIdR2012b(&st, &ab_emlrtRTEI,
      "Coder:FE:PotentialVectorVector", 0);
  }
}

/*
 *
 */
static void merge(const emlrtStack *sp, emxArray_int32_T *idx, emxArray_real_T
                  *x, int32_T offset, int32_T np, int32_T nq, emxArray_int32_T
                  *iwork, emxArray_real_T *xwork)
{
  int32_T n;
  int32_T qend;
  int32_T p;
  int32_T iout;
  int32_T exitg1;
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  if (nq != 0) {
    n = np + nq;
    st.site = &kb_emlrtRSI;
    if ((!(1 > n)) && (n > 2147483646)) {
      b_st.site = &i_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }

    for (qend = 0; qend + 1 <= n; qend++) {
      iwork->data[qend] = idx->data[offset + qend];
      xwork->data[qend] = x->data[offset + qend];
    }

    p = 0;
    n = np;
    qend = np + nq;
    iout = offset - 1;
    do {
      exitg1 = 0;
      iout++;
      if (xwork->data[p] <= xwork->data[n]) {
        idx->data[iout] = iwork->data[p];
        x->data[iout] = xwork->data[p];
        if (p + 1 < np) {
          p++;
        } else {
          exitg1 = 1;
        }
      } else {
        idx->data[iout] = iwork->data[n];
        x->data[iout] = xwork->data[n];
        if (n + 1 < qend) {
          n++;
        } else {
          n = (iout - p) + 1;
          st.site = &jb_emlrtRSI;
          if ((!(p + 1 > np)) && (np > 2147483646)) {
            b_st.site = &i_emlrtRSI;
            check_forloop_overflow_error(&b_st);
          }

          while (p + 1 <= np) {
            idx->data[n + p] = iwork->data[p];
            x->data[n + p] = xwork->data[p];
            p++;
          }

          exitg1 = 1;
        }
      }
    } while (exitg1 == 0);
  }
}

/*
 *
 */
static void merge_block(const emlrtStack *sp, emxArray_int32_T *idx,
  emxArray_real_T *x, int32_T offset, int32_T n, int32_T preSortLevel,
  emxArray_int32_T *iwork, emxArray_real_T *xwork)
{
  int32_T nPairs;
  int32_T bLen;
  int32_T tailOffset;
  int32_T nTail;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  nPairs = n >> preSortLevel;
  bLen = 1 << preSortLevel;
  while (nPairs > 1) {
    if ((nPairs & 1) != 0) {
      nPairs--;
      tailOffset = bLen * nPairs;
      nTail = n - tailOffset;
      if (nTail > bLen) {
        st.site = &fb_emlrtRSI;
        merge(&st, idx, x, offset + tailOffset, bLen, nTail - bLen, iwork, xwork);
      }
    }

    tailOffset = bLen << 1;
    nPairs >>= 1;
    for (nTail = 1; nTail <= nPairs; nTail++) {
      st.site = &hb_emlrtRSI;
      merge(&st, idx, x, offset + (nTail - 1) * tailOffset, bLen, bLen, iwork,
            xwork);
    }

    bLen = tailOffset;
  }

  if (n > bLen) {
    st.site = &ib_emlrtRSI;
    merge(&st, idx, x, offset, bLen, n - bLen, iwork, xwork);
  }
}

/*
 *
 */
static void sort(const emlrtStack *sp, emxArray_real_T *x, emxArray_int32_T *idx)
{
  int32_T dim;
  int32_T i1;
  emxArray_real_T *vwork;
  int32_T j;
  int32_T vstride;
  int32_T k;
  emxArray_int32_T *iidx;
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  dim = 2;
  if (x->size[0] != 1) {
    dim = 1;
  }

  st.site = &k_emlrtRSI;
  if (dim <= 1) {
    i1 = x->size[0];
  } else {
    i1 = 1;
  }

  emxInit_real_T(sp, &vwork, 1, &o_emlrtRTEI, true);
  j = vwork->size[0];
  vwork->size[0] = i1;
  emxEnsureCapacity(sp, (emxArray__common *)vwork, j, (int32_T)sizeof(real_T),
                    &k_emlrtRTEI);
  vstride = x->size[0];
  j = idx->size[0];
  idx->size[0] = vstride;
  emxEnsureCapacity(sp, (emxArray__common *)idx, j, (int32_T)sizeof(int32_T),
                    &k_emlrtRTEI);
  st.site = &l_emlrtRSI;
  vstride = 1;
  k = 1;
  while (k <= dim - 1) {
    vstride *= x->size[0];
    k = 2;
  }

  st.site = &m_emlrtRSI;
  st.site = &n_emlrtRSI;
  if ((!(1 > vstride)) && (vstride > 2147483646)) {
    b_st.site = &i_emlrtRSI;
    check_forloop_overflow_error(&b_st);
  }

  j = 0;
  emxInit_int32_T(sp, &iidx, 1, &k_emlrtRTEI, true);
  while (j + 1 <= vstride) {
    st.site = &o_emlrtRSI;
    if ((!(1 > i1)) && (i1 > 2147483646)) {
      b_st.site = &i_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }

    for (k = 0; k + 1 <= i1; k++) {
      vwork->data[k] = x->data[j + k * vstride];
    }

    st.site = &p_emlrtRSI;
    sortIdx(&st, vwork, iidx);
    st.site = &q_emlrtRSI;
    for (k = 0; k + 1 <= i1; k++) {
      x->data[j + k * vstride] = vwork->data[k];
      idx->data[j + k * vstride] = iidx->data[k];
    }

    j++;
  }

  emxFree_int32_T(&iidx);
  emxFree_real_T(&vwork);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/*
 *
 */
static void sortIdx(const emlrtStack *sp, emxArray_real_T *x, emxArray_int32_T
                    *idx)
{
  emxArray_real_T *b_x;
  int32_T ib;
  int32_T wOffset;
  int32_T i;
  int32_T n;
  real_T x4[4];
  int32_T idx4[4];
  emxArray_int32_T *iwork;
  emxArray_real_T *xwork;
  int32_T nNaNs;
  boolean_T overflow;
  int32_T k;
  int8_T perm[4];
  int32_T p;
  int32_T nNonNaN;
  int32_T i4;
  int32_T nBlocks;
  int32_T b_iwork[256];
  real_T b_xwork[256];
  int32_T bLen;
  int32_T bLen2;
  int32_T nPairs;
  int32_T exitg1;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emxInit_real_T(sp, &b_x, 1, &l_emlrtRTEI, true);
  ib = x->size[0];
  st.site = &s_emlrtRSI;
  b_st.site = &t_emlrtRSI;
  wOffset = b_x->size[0];
  b_x->size[0] = x->size[0];
  emxEnsureCapacity(&b_st, (emxArray__common *)b_x, wOffset, (int32_T)sizeof
                    (real_T), &l_emlrtRTEI);
  i = x->size[0];
  for (wOffset = 0; wOffset < i; wOffset++) {
    b_x->data[wOffset] = x->data[wOffset];
  }

  wOffset = idx->size[0];
  idx->size[0] = ib;
  emxEnsureCapacity(&b_st, (emxArray__common *)idx, wOffset, (int32_T)sizeof
                    (int32_T), &l_emlrtRTEI);
  for (wOffset = 0; wOffset < ib; wOffset++) {
    idx->data[wOffset] = 0;
  }

  n = x->size[0];
  for (i = 0; i < 4; i++) {
    x4[i] = 0.0;
    idx4[i] = 0;
  }

  emxInit_int32_T(&b_st, &iwork, 1, &r_emlrtRTEI, true);
  wOffset = iwork->size[0];
  iwork->size[0] = ib;
  emxEnsureCapacity(&b_st, (emxArray__common *)iwork, wOffset, (int32_T)sizeof
                    (int32_T), &p_emlrtRTEI);
  i = iwork->size[0];
  wOffset = iwork->size[0];
  iwork->size[0] = i;
  emxEnsureCapacity(&b_st, (emxArray__common *)iwork, wOffset, (int32_T)sizeof
                    (int32_T), &l_emlrtRTEI);
  for (wOffset = 0; wOffset < i; wOffset++) {
    iwork->data[wOffset] = 0;
  }

  emxInit_real_T(&b_st, &xwork, 1, &s_emlrtRTEI, true);
  i = x->size[0];
  wOffset = xwork->size[0];
  xwork->size[0] = i;
  emxEnsureCapacity(&b_st, (emxArray__common *)xwork, wOffset, (int32_T)sizeof
                    (real_T), &q_emlrtRTEI);
  i = xwork->size[0];
  wOffset = xwork->size[0];
  xwork->size[0] = i;
  emxEnsureCapacity(&b_st, (emxArray__common *)xwork, wOffset, (int32_T)sizeof
                    (real_T), &l_emlrtRTEI);
  for (wOffset = 0; wOffset < i; wOffset++) {
    xwork->data[wOffset] = 0.0;
  }

  nNaNs = 0;
  ib = 0;
  c_st.site = &y_emlrtRSI;
  overflow = ((!(1 > x->size[0])) && (x->size[0] > 2147483646));
  if (overflow) {
    d_st.site = &i_emlrtRSI;
    check_forloop_overflow_error(&d_st);
  }

  for (k = 0; k + 1 <= n; k++) {
    if (muDoubleScalarIsNaN(b_x->data[k])) {
      idx->data[(n - nNaNs) - 1] = k + 1;
      xwork->data[(n - nNaNs) - 1] = b_x->data[k];
      nNaNs++;
    } else {
      ib++;
      idx4[ib - 1] = k + 1;
      x4[ib - 1] = b_x->data[k];
      if (ib == 4) {
        i = k - nNaNs;
        if (x4[0] <= x4[1]) {
          ib = 1;
          wOffset = 2;
        } else {
          ib = 2;
          wOffset = 1;
        }

        if (x4[2] <= x4[3]) {
          p = 3;
          i4 = 4;
        } else {
          p = 4;
          i4 = 3;
        }

        if (x4[ib - 1] <= x4[p - 1]) {
          if (x4[wOffset - 1] <= x4[p - 1]) {
            perm[0] = (int8_T)ib;
            perm[1] = (int8_T)wOffset;
            perm[2] = (int8_T)p;
            perm[3] = (int8_T)i4;
          } else if (x4[wOffset - 1] <= x4[i4 - 1]) {
            perm[0] = (int8_T)ib;
            perm[1] = (int8_T)p;
            perm[2] = (int8_T)wOffset;
            perm[3] = (int8_T)i4;
          } else {
            perm[0] = (int8_T)ib;
            perm[1] = (int8_T)p;
            perm[2] = (int8_T)i4;
            perm[3] = (int8_T)wOffset;
          }
        } else if (x4[ib - 1] <= x4[i4 - 1]) {
          if (x4[wOffset - 1] <= x4[i4 - 1]) {
            perm[0] = (int8_T)p;
            perm[1] = (int8_T)ib;
            perm[2] = (int8_T)wOffset;
            perm[3] = (int8_T)i4;
          } else {
            perm[0] = (int8_T)p;
            perm[1] = (int8_T)ib;
            perm[2] = (int8_T)i4;
            perm[3] = (int8_T)wOffset;
          }
        } else {
          perm[0] = (int8_T)p;
          perm[1] = (int8_T)i4;
          perm[2] = (int8_T)ib;
          perm[3] = (int8_T)wOffset;
        }

        idx->data[i - 3] = idx4[perm[0] - 1];
        idx->data[i - 2] = idx4[perm[1] - 1];
        idx->data[i - 1] = idx4[perm[2] - 1];
        idx->data[i] = idx4[perm[3] - 1];
        b_x->data[i - 3] = x4[perm[0] - 1];
        b_x->data[i - 2] = x4[perm[1] - 1];
        b_x->data[i - 1] = x4[perm[2] - 1];
        b_x->data[i] = x4[perm[3] - 1];
        ib = 0;
      }
    }
  }

  wOffset = (x->size[0] - nNaNs) - 1;
  if (ib > 0) {
    for (i = 0; i < 4; i++) {
      perm[i] = 0;
    }

    if (ib == 1) {
      perm[0] = 1;
    } else if (ib == 2) {
      if (x4[0] <= x4[1]) {
        perm[0] = 1;
        perm[1] = 2;
      } else {
        perm[0] = 2;
        perm[1] = 1;
      }
    } else if (x4[0] <= x4[1]) {
      if (x4[1] <= x4[2]) {
        perm[0] = 1;
        perm[1] = 2;
        perm[2] = 3;
      } else if (x4[0] <= x4[2]) {
        perm[0] = 1;
        perm[1] = 3;
        perm[2] = 2;
      } else {
        perm[0] = 3;
        perm[1] = 1;
        perm[2] = 2;
      }
    } else if (x4[0] <= x4[2]) {
      perm[0] = 2;
      perm[1] = 1;
      perm[2] = 3;
    } else if (x4[1] <= x4[2]) {
      perm[0] = 2;
      perm[1] = 3;
      perm[2] = 1;
    } else {
      perm[0] = 3;
      perm[1] = 2;
      perm[2] = 1;
    }

    c_st.site = &ab_emlrtRSI;
    if (ib > 2147483646) {
      d_st.site = &i_emlrtRSI;
      check_forloop_overflow_error(&d_st);
    }

    for (k = 1; k <= ib; k++) {
      idx->data[(wOffset - ib) + k] = idx4[perm[k - 1] - 1];
      b_x->data[(wOffset - ib) + k] = x4[perm[k - 1] - 1];
    }
  }

  i = (nNaNs >> 1) + 1;
  c_st.site = &bb_emlrtRSI;
  for (k = 1; k < i; k++) {
    ib = idx->data[wOffset + k];
    idx->data[wOffset + k] = idx->data[n - k];
    idx->data[n - k] = ib;
    b_x->data[wOffset + k] = xwork->data[n - k];
    b_x->data[n - k] = xwork->data[wOffset + k];
  }

  if ((nNaNs & 1) != 0) {
    b_x->data[wOffset + i] = xwork->data[wOffset + i];
  }

  nNonNaN = x->size[0] - nNaNs;
  i = 2;
  if (nNonNaN > 1) {
    if (x->size[0] >= 256) {
      nBlocks = nNonNaN >> 8;
      if (nBlocks > 0) {
        b_st.site = &u_emlrtRSI;
        for (i4 = 1; i4 <= nBlocks; i4++) {
          b_st.site = &v_emlrtRSI;
          nNaNs = (i4 - 1) << 8;
          for (n = 0; n < 6; n++) {
            bLen = 1 << (n + 2);
            bLen2 = bLen << 1;
            nPairs = 256 >> (n + 3);
            c_st.site = &cb_emlrtRSI;
            for (k = 1; k <= nPairs; k++) {
              ib = nNaNs + (k - 1) * bLen2;
              c_st.site = &db_emlrtRSI;
              for (i = 0; i + 1 <= bLen2; i++) {
                b_iwork[i] = idx->data[ib + i];
                b_xwork[i] = b_x->data[ib + i];
              }

              p = 0;
              wOffset = bLen;
              i = ib - 1;
              do {
                exitg1 = 0;
                i++;
                if (b_xwork[p] <= b_xwork[wOffset]) {
                  idx->data[i] = b_iwork[p];
                  b_x->data[i] = b_xwork[p];
                  if (p + 1 < bLen) {
                    p++;
                  } else {
                    exitg1 = 1;
                  }
                } else {
                  idx->data[i] = b_iwork[wOffset];
                  b_x->data[i] = b_xwork[wOffset];
                  if (wOffset + 1 < bLen2) {
                    wOffset++;
                  } else {
                    i = (i - p) + 1;
                    c_st.site = &eb_emlrtRSI;
                    while (p + 1 <= bLen) {
                      idx->data[i + p] = b_iwork[p];
                      b_x->data[i + p] = b_xwork[p];
                      p++;
                    }

                    exitg1 = 1;
                  }
                }
              } while (exitg1 == 0);
            }
          }
        }

        i = nBlocks << 8;
        ib = nNonNaN - i;
        if (ib > 0) {
          b_st.site = &w_emlrtRSI;
          merge_block(&b_st, idx, b_x, i, ib, 2, iwork, xwork);
        }

        i = 8;
      }
    }

    b_st.site = &x_emlrtRSI;
    merge_block(&b_st, idx, b_x, 0, nNonNaN, i, iwork, xwork);
  }

  emxFree_real_T(&xwork);
  emxFree_int32_T(&iwork);
  wOffset = x->size[0];
  x->size[0] = b_x->size[0];
  emxEnsureCapacity(sp, (emxArray__common *)x, wOffset, (int32_T)sizeof(real_T),
                    &l_emlrtRTEI);
  i = b_x->size[0];
  for (wOffset = 0; wOffset < i; wOffset++) {
    x->data[wOffset] = b_x->data[wOffset];
  }

  emxFree_real_T(&b_x);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/*
 *
 */
static void squeeze(const emlrtStack *sp, const emxArray_real_T *a,
                    emxArray_real_T *b)
{
  int32_T k;
  int32_T sqsz[3];
  boolean_T overflow;
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  k = 3;
  while ((k > 2) && (a->size[2] == 1)) {
    k = 2;
  }

  if (k <= 2) {
    k = b->size[0];
    b->size[0] = 1;
    emxEnsureCapacity(sp, (emxArray__common *)b, k, (int32_T)sizeof(real_T),
                      &j_emlrtRTEI);
    st.site = &g_emlrtRSI;
    overflow = ((!(1 > a->size[2])) && (a->size[2] > 2147483646));
    if (overflow) {
      b_st.site = &i_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }

    for (k = 1; k <= a->size[2]; k++) {
      b->data[0] = a->data[k - 1];
    }
  } else {
    for (k = 0; k < 3; k++) {
      sqsz[k] = 1;
    }

    if (a->size[2] != 1) {
      sqsz[0] = a->size[2];
    }

    k = b->size[0];
    b->size[0] = sqsz[0];
    emxEnsureCapacity(sp, (emxArray__common *)b, k, (int32_T)sizeof(real_T),
                      &j_emlrtRTEI);
    st.site = &h_emlrtRSI;
    overflow = ((!(1 > a->size[2])) && (a->size[2] > 2147483646));
    if (overflow) {
      b_st.site = &i_emlrtRSI;
      check_forloop_overflow_error(&b_st);
    }

    for (k = 0; k + 1 <= a->size[2]; k++) {
      b->data[k] = a->data[k];
    }
  }
}

/*
 * function [CSH_ann,numUniqueResultsPerPixel,sortedErrors] = TransitiveKNN_part2(A,B,k,width,overSafeResults,errorMat)
 */
void TransitiveKNN_part2(const emlrtStack *sp, const emxArray_uint8_T *A, const
  emxArray_uint8_T *B, real_T k, real_T width, const emxArray_real_T
  *overSafeResults, const emxArray_real_T *errorMat, emxArray_real_T *CSH_ann,
  emxArray_real_T *numUniqueResultsPerPixel, emxArray_real_T *sortedErrors)
{
  int32_T i0;
  real_T d0;
  real_T d1;
  int32_T idx;
  emxArray_int32_T *b_CSH_ann;
  int32_T i;
  emxArray_real_T *OSR;
  emxArray_real_T *sOSR;
  emxArray_int32_T *uOSRinds;
  emxArray_int32_T *uOSRFinalinds;
  emxArray_int32_T *sInds;
  emxArray_int32_T *r0;
  emxArray_real_T *x;
  emxArray_boolean_T *b_x;
  emxArray_int32_T *ii;
  emxArray_int32_T *vk;
  emxArray_real_T *r1;
  emxArray_real_T *b_overSafeResults;
  emxArray_real_T *b_errorMat;
  emxArray_real_T *c_x;
  int32_T j;
  int32_T plast;
  int32_T isrc;
  uint32_T insz[4];
  uint32_T outsz[4];
  boolean_T overflow;
  int32_T iwork[4];
  boolean_T exitg2;
  boolean_T guard1 = false;
  static const int8_T iv0[4] = { 2, 3, 4, 1 };

  int32_T inc[4];
  static const int8_T iv1[4] = { 1, 2, 3, 0 };

  int32_T exitg1;
  boolean_T exitg4;
  boolean_T guard2 = false;
  int32_T iv2[2];
  int32_T iv3[2];
  uint32_T siz_idx_0;
  uint32_T siz_idx_1;
  int32_T exitg3;
  int32_T b_ii[1];
  int32_T c_ii[1];
  int32_T iv4[3];
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);

  /* 'TransitiveKNN_part2:3' [hA,wA,dA] = size(A); */
  /* 'TransitiveKNN_part2:4' [hB,wB,dB] = size(B); */
  /*  sort by errors and then take the k mappings of the k smallest [unique!] errors */
  /* 'TransitiveKNN_part2:7' br_boundary_to_ignore = width-1; */
  /* 'TransitiveKNN_part2:8' numUniqueResultsPerPixel = zeros(hA-br_boundary_to_ignore,wA-br_boundary_to_ignore); */
  i0 = numUniqueResultsPerPixel->size[0] * numUniqueResultsPerPixel->size[1];
  d0 = (real_T)A->size[0] - (width - 1.0);
  if (!(d0 >= 0.0)) {
    emlrtNonNegativeCheckR2012b(d0, &b_emlrtDCI, sp);
  }

  if (d0 != (int32_T)muDoubleScalarFloor(d0)) {
    emlrtIntegerCheckR2012b(d0, &emlrtDCI, sp);
  }

  numUniqueResultsPerPixel->size[0] = (int32_T)d0;
  d0 = (real_T)A->size[1] - (width - 1.0);
  if (!(d0 >= 0.0)) {
    emlrtNonNegativeCheckR2012b(d0, &d_emlrtDCI, sp);
  }

  if (d0 != (int32_T)muDoubleScalarFloor(d0)) {
    emlrtIntegerCheckR2012b(d0, &c_emlrtDCI, sp);
  }

  numUniqueResultsPerPixel->size[1] = (int32_T)d0;
  emxEnsureCapacity(sp, (emxArray__common *)numUniqueResultsPerPixel, i0,
                    (int32_T)sizeof(real_T), &emlrtRTEI);
  d0 = (real_T)A->size[0] - (width - 1.0);
  if (!(d0 >= 0.0)) {
    emlrtNonNegativeCheckR2012b(d0, &b_emlrtDCI, sp);
  }

  if (d0 != (int32_T)muDoubleScalarFloor(d0)) {
    emlrtIntegerCheckR2012b(d0, &emlrtDCI, sp);
  }

  d1 = (real_T)A->size[1] - (width - 1.0);
  if (!(d1 >= 0.0)) {
    emlrtNonNegativeCheckR2012b(d1, &d_emlrtDCI, sp);
  }

  if (d1 != (int32_T)muDoubleScalarFloor(d1)) {
    emlrtIntegerCheckR2012b(d1, &c_emlrtDCI, sp);
  }

  idx = (int32_T)d0 * (int32_T)d1;
  for (i0 = 0; i0 < idx; i0++) {
    numUniqueResultsPerPixel->data[i0] = 0.0;
  }

  emxInit_int32_T1(sp, &b_CSH_ann, 4, &g_emlrtRTEI, true);

  /* 'TransitiveKNN_part2:9' CSH_ann = zeros(k,hA,wA,2); */
  i0 = b_CSH_ann->size[0] * b_CSH_ann->size[1] * b_CSH_ann->size[2] *
    b_CSH_ann->size[3];
  if (!(k >= 0.0)) {
    emlrtNonNegativeCheckR2012b(k, &f_emlrtDCI, sp);
  }

  if (k != (int32_T)muDoubleScalarFloor(k)) {
    emlrtIntegerCheckR2012b(k, &e_emlrtDCI, sp);
  }

  b_CSH_ann->size[0] = (int32_T)k;
  b_CSH_ann->size[1] = A->size[0];
  b_CSH_ann->size[2] = A->size[1];
  b_CSH_ann->size[3] = 2;
  emxEnsureCapacity(sp, (emxArray__common *)b_CSH_ann, i0, (int32_T)sizeof
                    (int32_T), &emlrtRTEI);
  if (!(k >= 0.0)) {
    emlrtNonNegativeCheckR2012b(k, &f_emlrtDCI, sp);
  }

  if (k != (int32_T)muDoubleScalarFloor(k)) {
    emlrtIntegerCheckR2012b(k, &e_emlrtDCI, sp);
  }

  idx = (int32_T)k * A->size[0] * A->size[1] << 1;
  for (i0 = 0; i0 < idx; i0++) {
    b_CSH_ann->data[i0] = 0;
  }

  /* 'TransitiveKNN_part2:10' sortedErrors = zeros(hA,wA,k); */
  i0 = sortedErrors->size[0] * sortedErrors->size[1] * sortedErrors->size[2];
  sortedErrors->size[0] = A->size[0];
  sortedErrors->size[1] = A->size[1];
  if (k != (int32_T)muDoubleScalarFloor(k)) {
    emlrtIntegerCheckR2012b(k, &g_emlrtDCI, sp);
  }

  sortedErrors->size[2] = (int32_T)k;
  emxEnsureCapacity(sp, (emxArray__common *)sortedErrors, i0, (int32_T)sizeof
                    (real_T), &emlrtRTEI);
  if (k != (int32_T)muDoubleScalarFloor(k)) {
    emlrtIntegerCheckR2012b(k, &g_emlrtDCI, sp);
  }

  idx = A->size[0] * A->size[1] * (int32_T)k;
  for (i0 = 0; i0 < idx; i0++) {
    sortedErrors->data[i0] = 0.0;
  }

  /*  CSH_errors = zeros(hA,wA,k); */
  /* 'TransitiveKNN_part2:12' for i = 1 : hA-br_boundary_to_ignore */
  d0 = (real_T)A->size[0] - (width - 1.0);
  emlrtForLoopVectorCheckR2012b(1.0, 1.0, d0, mxDOUBLE_CLASS, (int32_T)d0,
    &t_emlrtRTEI, sp);
  i = 0;
  emxInit_real_T(sp, &OSR, 1, &c_emlrtRTEI, true);
  emxInit_real_T(sp, &sOSR, 1, &d_emlrtRTEI, true);
  emxInit_int32_T(sp, &uOSRinds, 1, &e_emlrtRTEI, true);
  emxInit_int32_T(sp, &uOSRFinalinds, 1, &f_emlrtRTEI, true);
  emxInit_int32_T(sp, &sInds, 1, &emlrtRTEI, true);
  emxInit_int32_T2(sp, &r0, 2, &emlrtRTEI, true);
  emxInit_real_T(sp, &x, 1, &emlrtRTEI, true);
  emxInit_boolean_T(sp, &b_x, 1, &emlrtRTEI, true);
  emxInit_int32_T(sp, &ii, 1, &h_emlrtRTEI, true);
  emxInit_int32_T(sp, &vk, 1, &i_emlrtRTEI, true);
  emxInit_real_T(sp, &r1, 1, &emlrtRTEI, true);
  emxInit_real_T1(sp, &b_overSafeResults, 3, &emlrtRTEI, true);
  emxInit_real_T1(sp, &b_errorMat, 3, &emlrtRTEI, true);
  emxInit_real_T(sp, &c_x, 1, &emlrtRTEI, true);
  while (i <= (int32_T)d0 - 1) {
    /* 'TransitiveKNN_part2:13' for j = 1 : wA-br_boundary_to_ignore */
    d1 = (real_T)A->size[1] - (width - 1.0);
    emlrtForLoopVectorCheckR2012b(1.0, 1.0, d1, mxDOUBLE_CLASS, (int32_T)d1,
      &u_emlrtRTEI, sp);
    j = 0;
    while (j <= (int32_T)d1 - 1) {
      /* 'TransitiveKNN_part2:14' [sErrors,sInds] = sort(squeeze(errorMat(i,j,:))); */
      idx = errorMat->size[2];
      i0 = errorMat->size[1];
      if (!((j + 1 >= 1) && (j + 1 <= i0))) {
        emlrtDynamicBoundsCheckR2012b(j + 1, 1, i0, &b_emlrtBCI, sp);
      }

      plast = j + 1;
      i0 = errorMat->size[0];
      if (!((i + 1 >= 1) && (i + 1 <= i0))) {
        emlrtDynamicBoundsCheckR2012b(i + 1, 1, i0, &emlrtBCI, sp);
      }

      isrc = i + 1;
      i0 = b_errorMat->size[0] * b_errorMat->size[1] * b_errorMat->size[2];
      b_errorMat->size[0] = 1;
      b_errorMat->size[1] = 1;
      b_errorMat->size[2] = idx;
      emxEnsureCapacity(sp, (emxArray__common *)b_errorMat, i0, (int32_T)sizeof
                        (real_T), &emlrtRTEI);
      for (i0 = 0; i0 < idx; i0++) {
        b_errorMat->data[b_errorMat->size[0] * b_errorMat->size[1] * i0] =
          errorMat->data[((isrc + errorMat->size[0] * (plast - 1)) +
                          errorMat->size[0] * errorMat->size[1] * i0) - 1];
      }

      st.site = &emlrtRSI;
      squeeze(&st, b_errorMat, x);
      st.site = &emlrtRSI;
      b_st.site = &j_emlrtRSI;
      sort(&b_st, x, ii);
      i0 = sInds->size[0];
      sInds->size[0] = ii->size[0];
      emxEnsureCapacity(&st, (emxArray__common *)sInds, i0, (int32_T)sizeof
                        (int32_T), &emlrtRTEI);
      idx = ii->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        sInds->data[i0] = ii->data[i0];
      }

      /* 'TransitiveKNN_part2:15' OSR = squeeze(overSafeResults(i,j,:)); */
      idx = overSafeResults->size[2];
      i0 = overSafeResults->size[1];
      if (!((j + 1 >= 1) && (j + 1 <= i0))) {
        emlrtDynamicBoundsCheckR2012b(j + 1, 1, i0, &d_emlrtBCI, sp);
      }

      plast = j + 1;
      i0 = overSafeResults->size[0];
      if (!((i + 1 >= 1) && (i + 1 <= i0))) {
        emlrtDynamicBoundsCheckR2012b(i + 1, 1, i0, &c_emlrtBCI, sp);
      }

      isrc = i + 1;
      i0 = b_overSafeResults->size[0] * b_overSafeResults->size[1] *
        b_overSafeResults->size[2];
      b_overSafeResults->size[0] = 1;
      b_overSafeResults->size[1] = 1;
      b_overSafeResults->size[2] = idx;
      emxEnsureCapacity(sp, (emxArray__common *)b_overSafeResults, i0, (int32_T)
                        sizeof(real_T), &emlrtRTEI);
      for (i0 = 0; i0 < idx; i0++) {
        b_overSafeResults->data[b_overSafeResults->size[0] *
          b_overSafeResults->size[1] * i0] = overSafeResults->data[((isrc +
          overSafeResults->size[0] * (plast - 1)) + overSafeResults->size[0] *
          overSafeResults->size[1] * i0) - 1];
      }

      st.site = &b_emlrtRSI;
      squeeze(&st, b_overSafeResults, OSR);

      /* 'TransitiveKNN_part2:16' sOSR = OSR(sInds); */
      plast = OSR->size[0];
      i0 = sOSR->size[0];
      sOSR->size[0] = ii->size[0];
      emxEnsureCapacity(sp, (emxArray__common *)sOSR, i0, (int32_T)sizeof(real_T),
                        &emlrtRTEI);
      idx = ii->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        isrc = ii->data[i0];
        if (!((isrc >= 1) && (isrc <= plast))) {
          emlrtDynamicBoundsCheckR2012b(isrc, 1, plast, &n_emlrtBCI, sp);
        }

        sOSR->data[i0] = OSR->data[isrc - 1];
      }

      /* 'TransitiveKNN_part2:17' uOSRinds = find(diff(sOSR)~=0); */
      st.site = &c_emlrtRSI;
      b_st.site = &c_emlrtRSI;
      diff(&b_st, sOSR, r1);
      i0 = b_x->size[0];
      b_x->size[0] = r1->size[0];
      emxEnsureCapacity(&st, (emxArray__common *)b_x, i0, (int32_T)sizeof
                        (boolean_T), &emlrtRTEI);
      idx = r1->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        b_x->data[i0] = (r1->data[i0] != 0.0);
      }

      b_st.site = &nb_emlrtRSI;
      isrc = b_x->size[0];
      idx = 0;
      i0 = ii->size[0];
      ii->size[0] = b_x->size[0];
      emxEnsureCapacity(&b_st, (emxArray__common *)ii, i0, (int32_T)sizeof
                        (int32_T), &emlrtRTEI);
      c_st.site = &ob_emlrtRSI;
      overflow = ((!(1 > b_x->size[0])) && (b_x->size[0] > 2147483646));
      if (overflow) {
        d_st.site = &i_emlrtRSI;
        check_forloop_overflow_error(&d_st);
      }

      plast = 1;
      exitg4 = false;
      while ((!exitg4) && (plast <= isrc)) {
        guard2 = false;
        if (b_x->data[plast - 1]) {
          idx++;
          ii->data[idx - 1] = plast;
          if (idx >= isrc) {
            exitg4 = true;
          } else {
            guard2 = true;
          }
        } else {
          guard2 = true;
        }

        if (guard2) {
          plast++;
        }
      }

      if (!(idx <= b_x->size[0])) {
        emlrtErrorWithMessageIdR2012b(&b_st, &v_emlrtRTEI,
          "Coder:builtins:AssertionFailed", 0);
      }

      if (b_x->size[0] == 1) {
        if (idx == 0) {
          i0 = ii->size[0];
          ii->size[0] = 0;
          emxEnsureCapacity(&b_st, (emxArray__common *)ii, i0, (int32_T)sizeof
                            (int32_T), &emlrtRTEI);
        }
      } else {
        if (1 > idx) {
          i0 = 0;
        } else {
          i0 = idx;
        }

        iv2[0] = 1;
        iv2[1] = i0;
        c_st.site = &pb_emlrtRSI;
        indexShapeCheck(&c_st, ii->size[0], iv2);
        isrc = ii->size[0];
        ii->size[0] = i0;
        emxEnsureCapacity(&b_st, (emxArray__common *)ii, isrc, (int32_T)sizeof
                          (int32_T), &b_emlrtRTEI);
      }

      i0 = uOSRinds->size[0];
      uOSRinds->size[0] = ii->size[0];
      emxEnsureCapacity(&st, (emxArray__common *)uOSRinds, i0, (int32_T)sizeof
                        (int32_T), &emlrtRTEI);
      idx = ii->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        uOSRinds->data[i0] = ii->data[i0];
      }

      /*  giving up (possibly) on the last index (diff gives a 1-shorter vector) */
      /* 'TransitiveKNN_part2:18' kActual = length(uOSRinds); */
      /* 'TransitiveKNN_part2:19' uOSRFinalinds = zeros(k,1); */
      i0 = uOSRFinalinds->size[0];
      if (k != (int32_T)muDoubleScalarFloor(k)) {
        emlrtIntegerCheckR2012b(k, &h_emlrtDCI, sp);
      }

      uOSRFinalinds->size[0] = (int32_T)k;
      emxEnsureCapacity(sp, (emxArray__common *)uOSRFinalinds, i0, (int32_T)
                        sizeof(int32_T), &emlrtRTEI);
      if (k != (int32_T)muDoubleScalarFloor(k)) {
        emlrtIntegerCheckR2012b(k, &h_emlrtDCI, sp);
      }

      idx = (int32_T)k;
      for (i0 = 0; i0 < idx; i0++) {
        uOSRFinalinds->data[i0] = 0;
      }

      /* 'TransitiveKNN_part2:20' numUniqueResultsPerPixel(i,j) = min(k,kActual); */
      i0 = numUniqueResultsPerPixel->size[0];
      if (!((i + 1 >= 1) && (i + 1 <= i0))) {
        emlrtDynamicBoundsCheckR2012b(i + 1, 1, i0, &o_emlrtBCI, sp);
      }

      i0 = numUniqueResultsPerPixel->size[1];
      if (!((j + 1 >= 1) && (j + 1 <= i0))) {
        emlrtDynamicBoundsCheckR2012b(j + 1, 1, i0, &p_emlrtBCI, sp);
      }

      numUniqueResultsPerPixel->data[i + numUniqueResultsPerPixel->size[0] * j] =
        muDoubleScalarMin(k, uOSRinds->size[0]);

      /* 'TransitiveKNN_part2:21' if kActual >= k */
      if (uOSRinds->size[0] >= k) {
        /* 'TransitiveKNN_part2:22' uOSRFinalinds = uOSRinds(1:k); */
        if (1.0 > k) {
          idx = 0;
        } else {
          i0 = uOSRinds->size[0];
          if (!(1 <= i0)) {
            emlrtDynamicBoundsCheckR2012b(1, 1, i0, &e_emlrtBCI, sp);
          }

          i0 = uOSRinds->size[0];
          idx = (int32_T)k;
          if (!(idx <= i0)) {
            emlrtDynamicBoundsCheckR2012b(idx, 1, i0, &e_emlrtBCI, sp);
          }
        }

        iv3[0] = 1;
        iv3[1] = idx;
        st.site = &d_emlrtRSI;
        indexShapeCheck(&st, uOSRinds->size[0], iv3);
        i0 = uOSRFinalinds->size[0];
        uOSRFinalinds->size[0] = idx;
        emxEnsureCapacity(sp, (emxArray__common *)uOSRFinalinds, i0, (int32_T)
                          sizeof(int32_T), &emlrtRTEI);
        for (i0 = 0; i0 < idx; i0++) {
          uOSRFinalinds->data[i0] = uOSRinds->data[i0];
        }
      } else {
        /* 'TransitiveKNN_part2:23' else */
        /* 'TransitiveKNN_part2:24' uOSRFinalinds(1:kActual) = uOSRinds; */
        if (1 > uOSRinds->size[0]) {
          idx = 0;
        } else {
          i0 = (int32_T)k;
          if (!(1 <= i0)) {
            emlrtDynamicBoundsCheckR2012b(1, 1, i0, &f_emlrtBCI, sp);
          }

          i0 = (int32_T)k;
          idx = uOSRinds->size[0];
          if (!((idx >= 1) && (idx <= i0))) {
            emlrtDynamicBoundsCheckR2012b(idx, 1, i0, &f_emlrtBCI, sp);
          }
        }

        i0 = uOSRinds->size[0];
        if (idx != i0) {
          emlrtSizeEqCheck1DR2012b(idx, i0, &emlrtECI, sp);
        }

        i0 = r0->size[0] * r0->size[1];
        r0->size[0] = 1;
        r0->size[1] = idx;
        emxEnsureCapacity(sp, (emxArray__common *)r0, i0, (int32_T)sizeof
                          (int32_T), &emlrtRTEI);
        for (i0 = 0; i0 < idx; i0++) {
          r0->data[r0->size[0] * i0] = i0;
        }

        idx = r0->size[0] * r0->size[1];
        for (i0 = 0; i0 < idx; i0++) {
          uOSRFinalinds->data[r0->data[i0]] = uOSRinds->data[i0];
        }

        /* 'TransitiveKNN_part2:25' uOSRFinalinds(kActual+1:k) = uOSRinds(end); */
        if ((real_T)uOSRinds->size[0] + 1.0 > k) {
          i0 = 0;
          plast = 0;
        } else {
          i0 = uOSRFinalinds->size[0];
          isrc = (int32_T)(uOSRinds->size[0] + 1U);
          if (!((isrc >= 1) && (isrc <= i0))) {
            emlrtDynamicBoundsCheckR2012b(isrc, 1, i0, &g_emlrtBCI, sp);
          }

          i0 = isrc - 1;
          isrc = uOSRFinalinds->size[0];
          plast = (int32_T)k;
          if (!((plast >= 1) && (plast <= isrc))) {
            emlrtDynamicBoundsCheckR2012b(plast, 1, isrc, &g_emlrtBCI, sp);
          }
        }

        isrc = r0->size[0] * r0->size[1];
        r0->size[0] = 1;
        r0->size[1] = plast - i0;
        emxEnsureCapacity(sp, (emxArray__common *)r0, isrc, (int32_T)sizeof
                          (int32_T), &emlrtRTEI);
        idx = plast - i0;
        for (isrc = 0; isrc < idx; isrc++) {
          r0->data[r0->size[0] * isrc] = i0 + isrc;
        }

        idx = r0->size[0] * r0->size[1];
        for (i0 = 0; i0 < idx; i0++) {
          isrc = uOSRinds->size[0];
          plast = uOSRinds->size[0];
          if (!((plast >= 1) && (plast <= isrc))) {
            emlrtDynamicBoundsCheckR2012b(plast, 1, isrc, &q_emlrtBCI, sp);
          }

          uOSRFinalinds->data[r0->data[i0]] = uOSRinds->data[plast - 1];
        }

        /*  returning duplicate copies in this case */
      }

      /*  NN results: */
      /* 'TransitiveKNN_part2:28' [CSH_ann(:,i,j,2),CSH_ann(:,i,j,1)] = ind2sub([hB,wB],sOSR(uOSRFinalinds)); */
      st.site = &e_emlrtRSI;
      siz_idx_0 = (uint32_T)B->size[0];
      siz_idx_1 = (uint32_T)B->size[1];
      plast = sInds->size[0];
      idx = uOSRFinalinds->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        isrc = uOSRFinalinds->data[i0];
        if (!((isrc >= 1) && (isrc <= plast))) {
          emlrtDynamicBoundsCheckR2012b(isrc, 1, plast, &r_emlrtBCI, &st);
        }
      }

      b_st.site = &rb_emlrtRSI;
      i0 = ii->size[0];
      ii->size[0] = uOSRFinalinds->size[0];
      emxEnsureCapacity(&b_st, (emxArray__common *)ii, i0, (int32_T)sizeof
                        (int32_T), &emlrtRTEI);
      idx = uOSRFinalinds->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        ii->data[i0] = (int32_T)sOSR->data[uOSRFinalinds->data[i0] - 1];
      }

      plast = (int32_T)siz_idx_0 * (int32_T)siz_idx_1;
      idx = 0;
      do {
        exitg3 = 0;
        if (idx <= ii->size[0] - 1) {
          if ((ii->data[idx] >= 1) && (ii->data[idx] <= plast)) {
            overflow = true;
          } else {
            overflow = false;
          }

          if (!overflow) {
            overflow = false;
            exitg3 = 1;
          } else {
            idx++;
          }
        } else {
          overflow = true;
          exitg3 = 1;
        }
      } while (exitg3 == 0);

      if (!overflow) {
        emlrtErrorWithMessageIdR2012b(&b_st, &w_emlrtRTEI,
          "Coder:MATLAB:ind2sub_IndexOutOfRange", 0);
      }

      i0 = ii->size[0];
      emxEnsureCapacity(&b_st, (emxArray__common *)ii, i0, (int32_T)sizeof
                        (int32_T), &emlrtRTEI);
      idx = ii->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        ii->data[i0]--;
      }

      i0 = vk->size[0];
      vk->size[0] = ii->size[0];
      emxEnsureCapacity(&b_st, (emxArray__common *)vk, i0, (int32_T)sizeof
                        (int32_T), &emlrtRTEI);
      idx = ii->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        c_st.site = &ub_emlrtRSI;
        vk->data[i0] = div_s32(&c_st, ii->data[i0], (int32_T)siz_idx_0);
      }

      i0 = ii->size[0];
      emxEnsureCapacity(&b_st, (emxArray__common *)ii, i0, (int32_T)sizeof
                        (int32_T), &emlrtRTEI);
      idx = ii->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        ii->data[i0] -= vk->data[i0] * (int32_T)siz_idx_0;
      }

      i0 = r1->size[0];
      r1->size[0] = ii->size[0];
      emxEnsureCapacity(&st, (emxArray__common *)r1, i0, (int32_T)sizeof(real_T),
                        &emlrtRTEI);
      idx = ii->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        r1->data[i0] = ii->data[i0] + 1;
      }

      i0 = uOSRinds->size[0];
      uOSRinds->size[0] = vk->size[0];
      emxEnsureCapacity(&st, (emxArray__common *)uOSRinds, i0, (int32_T)sizeof
                        (int32_T), &emlrtRTEI);
      idx = vk->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        uOSRinds->data[i0] = vk->data[i0] + 1;
      }

      idx = b_CSH_ann->size[0];
      i0 = ii->size[0];
      ii->size[0] = idx;
      emxEnsureCapacity(sp, (emxArray__common *)ii, i0, (int32_T)sizeof(int32_T),
                        &emlrtRTEI);
      for (i0 = 0; i0 < idx; i0++) {
        ii->data[i0] = i0;
      }

      i0 = b_CSH_ann->size[1];
      isrc = i + 1;
      if (!((isrc >= 1) && (isrc <= i0))) {
        emlrtDynamicBoundsCheckR2012b(isrc, 1, i0, &h_emlrtBCI, sp);
      }

      i0 = b_CSH_ann->size[2];
      isrc = j + 1;
      if (!((isrc >= 1) && (isrc <= i0))) {
        emlrtDynamicBoundsCheckR2012b(isrc, 1, i0, &i_emlrtBCI, sp);
      }

      b_ii[0] = ii->size[0];
      emlrtSubAssignSizeCheckR2012b(b_ii, 1, *(int32_T (*)[1])r1->size, 1,
        &b_emlrtECI, sp);
      idx = r1->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        b_CSH_ann->data[((ii->data[i0] + b_CSH_ann->size[0] * i) +
                         b_CSH_ann->size[0] * b_CSH_ann->size[1] * j) +
          b_CSH_ann->size[0] * b_CSH_ann->size[1] * b_CSH_ann->size[2]] =
          (int32_T)r1->data[i0];
      }

      idx = b_CSH_ann->size[0];
      i0 = ii->size[0];
      ii->size[0] = idx;
      emxEnsureCapacity(sp, (emxArray__common *)ii, i0, (int32_T)sizeof(int32_T),
                        &emlrtRTEI);
      for (i0 = 0; i0 < idx; i0++) {
        ii->data[i0] = i0;
      }

      i0 = b_CSH_ann->size[1];
      isrc = i + 1;
      if (!((isrc >= 1) && (isrc <= i0))) {
        emlrtDynamicBoundsCheckR2012b(isrc, 1, i0, &j_emlrtBCI, sp);
      }

      i0 = b_CSH_ann->size[2];
      isrc = j + 1;
      if (!((isrc >= 1) && (isrc <= i0))) {
        emlrtDynamicBoundsCheckR2012b(isrc, 1, i0, &k_emlrtBCI, sp);
      }

      c_ii[0] = ii->size[0];
      emlrtSubAssignSizeCheckR2012b(c_ii, 1, *(int32_T (*)[1])uOSRinds->size, 1,
        &c_emlrtECI, sp);
      idx = uOSRinds->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        b_CSH_ann->data[(ii->data[i0] + b_CSH_ann->size[0] * i) +
          b_CSH_ann->size[0] * b_CSH_ann->size[1] * j] = uOSRinds->data[i0];
      }

      /*  sorted errors: */
      /* 'TransitiveKNN_part2:30' sortedErrors(i,j,:) = sErrors(uOSRFinalinds); */
      i0 = sortedErrors->size[0];
      isrc = i + 1;
      if (!((isrc >= 1) && (isrc <= i0))) {
        emlrtDynamicBoundsCheckR2012b(isrc, 1, i0, &l_emlrtBCI, sp);
      }

      i0 = sortedErrors->size[1];
      isrc = j + 1;
      if (!((isrc >= 1) && (isrc <= i0))) {
        emlrtDynamicBoundsCheckR2012b(isrc, 1, i0, &m_emlrtBCI, sp);
      }

      idx = sortedErrors->size[2];
      i0 = ii->size[0];
      ii->size[0] = idx;
      emxEnsureCapacity(sp, (emxArray__common *)ii, i0, (int32_T)sizeof(int32_T),
                        &emlrtRTEI);
      for (i0 = 0; i0 < idx; i0++) {
        ii->data[i0] = i0;
      }

      plast = x->size[0];
      idx = uOSRFinalinds->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        isrc = uOSRFinalinds->data[i0];
        if (!((isrc >= 1) && (isrc <= plast))) {
          emlrtDynamicBoundsCheckR2012b(isrc, 1, plast, &s_emlrtBCI, sp);
        }
      }

      iv4[0] = 1;
      iv4[1] = 1;
      iv4[2] = ii->size[0];
      emlrtSubAssignSizeCheckR2012b(iv4, 3, *(int32_T (*)[1])uOSRFinalinds->size,
        1, &d_emlrtECI, sp);
      i0 = c_x->size[0];
      c_x->size[0] = uOSRFinalinds->size[0];
      emxEnsureCapacity(sp, (emxArray__common *)c_x, i0, (int32_T)sizeof(real_T),
                        &emlrtRTEI);
      idx = uOSRFinalinds->size[0];
      for (i0 = 0; i0 < idx; i0++) {
        c_x->data[i0] = x->data[uOSRFinalinds->data[i0] - 1];
      }

      plast = ii->size[0];
      for (i0 = 0; i0 < plast; i0++) {
        sortedErrors->data[(i + sortedErrors->size[0] * j) + sortedErrors->size
          [0] * sortedErrors->size[1] * ii->data[i0]] = c_x->data[i0];
      }

      j++;
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    i++;
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFree_real_T(&c_x);
  emxFree_real_T(&b_errorMat);
  emxFree_real_T(&b_overSafeResults);
  emxFree_real_T(&r1);
  emxFree_int32_T(&vk);
  emxFree_int32_T(&ii);
  emxFree_boolean_T(&b_x);
  emxFree_real_T(&x);
  emxFree_int32_T(&r0);
  emxFree_int32_T(&sInds);
  emxFree_int32_T(&uOSRFinalinds);
  emxFree_int32_T(&uOSRinds);
  emxFree_real_T(&sOSR);
  emxFree_real_T(&OSR);

  /* 'TransitiveKNN_part2:34' CSH_ann = permute(CSH_ann,[2,3,4,1]); */
  st.site = &f_emlrtRSI;
  for (i0 = 0; i0 < 4; i0++) {
    insz[i0] = (uint32_T)b_CSH_ann->size[i0];
  }

  outsz[0] = insz[1];
  outsz[1] = insz[2];
  outsz[2] = 2U;
  outsz[3] = insz[0];
  i0 = CSH_ann->size[0] * CSH_ann->size[1] * CSH_ann->size[2] * CSH_ann->size[3];
  CSH_ann->size[0] = (int32_T)insz[1];
  CSH_ann->size[1] = (int32_T)insz[2];
  CSH_ann->size[2] = 2;
  CSH_ann->size[3] = (int32_T)insz[0];
  emxEnsureCapacity(&st, (emxArray__common *)CSH_ann, i0, (int32_T)sizeof(real_T),
                    &emlrtRTEI);
  overflow = true;
  if (!((b_CSH_ann->size[0] == 0) || (b_CSH_ann->size[1] == 0) ||
        (b_CSH_ann->size[2] == 0))) {
    plast = 0;
    idx = 0;
    exitg2 = false;
    while ((!exitg2) && (idx + 1 < 5)) {
      guard1 = false;
      if (b_CSH_ann->size[iv0[idx] - 1] != 1) {
        if (plast > iv0[idx]) {
          overflow = false;
          exitg2 = true;
        } else {
          plast = iv0[idx];
          guard1 = true;
        }
      } else {
        guard1 = true;
      }

      if (guard1) {
        idx++;
      }
    }
  }

  if (overflow) {
    plast = b_CSH_ann->size[0] * b_CSH_ann->size[1] * b_CSH_ann->size[2] << 1;
    b_st.site = &sb_emlrtRSI;
    if ((!(1 > plast)) && (plast > 2147483646)) {
      c_st.site = &i_emlrtRSI;
      check_forloop_overflow_error(&c_st);
    }

    for (idx = 0; idx + 1 <= plast; idx++) {
      CSH_ann->data[idx] = b_CSH_ann->data[idx];
    }
  } else {
    for (i0 = 0; i0 < 4; i0++) {
      iwork[i0] = 1;
    }

    for (idx = 0; idx < 3; idx++) {
      iwork[idx + 1] = iwork[idx] * (int32_T)insz[idx];
    }

    for (i0 = 0; i0 < 4; i0++) {
      inc[i0] = iwork[iv1[i0]];
    }

    for (i0 = 0; i0 < 4; i0++) {
      iwork[i0] = 0;
    }

    plast = 0;
    do {
      isrc = 0;
      for (idx = 0; idx < 3; idx++) {
        isrc += iwork[idx + 1] * inc[idx + 1];
      }

      b_st.site = &tb_emlrtRSI;
      if ((!(1 > (int32_T)outsz[0])) && ((int32_T)outsz[0] > 2147483646)) {
        c_st.site = &i_emlrtRSI;
        check_forloop_overflow_error(&c_st);
      }

      for (idx = 1; idx <= (int32_T)outsz[0]; idx++) {
        CSH_ann->data[plast] = b_CSH_ann->data[isrc];
        plast++;
        isrc += inc[0];
      }

      idx = 1;
      do {
        exitg1 = 0;
        iwork[idx]++;
        if (iwork[idx] < (int32_T)outsz[idx]) {
          exitg1 = 2;
        } else if (idx + 1 == 4) {
          exitg1 = 1;
        } else {
          iwork[idx] = 0;
          idx++;
        }
      } while (exitg1 == 0);
    } while (!(exitg1 == 1));
  }

  emxFree_int32_T(&b_CSH_ann);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

void TransitiveKNN_part2_api(const mxArray * const prhs[6], const mxArray *plhs
  [3])
{
  emxArray_uint8_T *A;
  emxArray_uint8_T *B;
  emxArray_real_T *overSafeResults;
  emxArray_real_T *errorMat;
  emxArray_real_T *CSH_ann;
  emxArray_real_T *numUniqueResultsPerPixel;
  emxArray_real_T *sortedErrors;
  real_T k;
  real_T width;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtHeapReferenceStackEnterFcnR2012b(&st);
  emxInit_uint8_T(&st, &A, 3, &n_emlrtRTEI, true);
  emxInit_uint8_T(&st, &B, 3, &n_emlrtRTEI, true);
  emxInit_real_T1(&st, &overSafeResults, 3, &n_emlrtRTEI, true);
  emxInit_real_T1(&st, &errorMat, 3, &n_emlrtRTEI, true);
  emxInit_real_T2(&st, &CSH_ann, 4, &n_emlrtRTEI, true);
  emxInit_real_T3(&st, &numUniqueResultsPerPixel, 2, &n_emlrtRTEI, true);
  emxInit_real_T1(&st, &sortedErrors, 3, &n_emlrtRTEI, true);

  /* Marshall function inputs */
  emlrt_marshallIn(&st, emlrtAlias(prhs[0]), "A", A);
  emlrt_marshallIn(&st, emlrtAlias(prhs[1]), "B", B);
  k = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "k");
  width = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[3]), "width");
  e_emlrt_marshallIn(&st, emlrtAlias(prhs[4]), "overSafeResults",
                     overSafeResults);
  e_emlrt_marshallIn(&st, emlrtAlias(prhs[5]), "errorMat", errorMat);

  /* Invoke the target function */
  TransitiveKNN_part2(&st, A, B, k, width, overSafeResults, errorMat, CSH_ann,
                      numUniqueResultsPerPixel, sortedErrors);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(CSH_ann);
  plhs[1] = b_emlrt_marshallOut(numUniqueResultsPerPixel);
  plhs[2] = c_emlrt_marshallOut(sortedErrors);
  sortedErrors->canFreeData = false;
  emxFree_real_T(&sortedErrors);
  numUniqueResultsPerPixel->canFreeData = false;
  emxFree_real_T(&numUniqueResultsPerPixel);
  CSH_ann->canFreeData = false;
  emxFree_real_T(&CSH_ann);
  errorMat->canFreeData = false;
  emxFree_real_T(&errorMat);
  overSafeResults->canFreeData = false;
  emxFree_real_T(&overSafeResults);
  B->canFreeData = false;
  emxFree_uint8_T(&B);
  A->canFreeData = false;
  emxFree_uint8_T(&A);
  emlrtHeapReferenceStackLeaveFcnR2012b(&st);
}

void TransitiveKNN_part2_atexit(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

void TransitiveKNN_part2_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

void TransitiveKNN_part2_terminate(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

mxArray *emlrtMexFcnProperties(void)
{
  mxArray *xResult;
  mxArray *xEntryPoints;
  const char * fldNames[4] = { "Name", "NumberOfInputs", "NumberOfOutputs",
    "ConstantInputs" };

  mxArray *xInputs;
  const char * b_fldNames[4] = { "Version", "ResolvedFunctions", "EntryPoints",
    "CoverageInfo" };

  xEntryPoints = emlrtCreateStructMatrix(1, 1, 4, fldNames);
  xInputs = emlrtCreateLogicalMatrix(1, 6);
  emlrtSetField(xEntryPoints, 0, "Name", mxCreateString("TransitiveKNN_part2"));
  emlrtSetField(xEntryPoints, 0, "NumberOfInputs", mxCreateDoubleScalar(6.0));
  emlrtSetField(xEntryPoints, 0, "NumberOfOutputs", mxCreateDoubleScalar(3.0));
  emlrtSetField(xEntryPoints, 0, "ConstantInputs", xInputs);
  xResult = emlrtCreateStructMatrix(1, 1, 4, b_fldNames);
  emlrtSetField(xResult, 0, "Version", mxCreateString("9.1.0.441655 (R2016b)"));
  emlrtSetField(xResult, 0, "ResolvedFunctions", (mxArray *)
                emlrtMexFcnResolvedFunctionsInfo());
  emlrtSetField(xResult, 0, "EntryPoints", xEntryPoints);
  return xResult;
}

const mxArray *emlrtMexFcnResolvedFunctionsInfo(void)
{
  const mxArray *nameCaptureInfo;
  const char * data[40] = {
    "789ced5d4b6c234972adde99ed9d39ccee7806b677fcddf62e1663b7d1a4445112073e343fc59f48f153a4f899e9e5148b4532c5fab1aaf8d3898001a32fc6ee"
    "d107fbbeb061788feba36118b00f7bb27df7d13e18f0c177bb8a1f35954d4d52f54d752501420a4a998c08e66344464446524f7245ca787cd778feece609f5d4",
    "f8f981f1fc16b57e7c7b433f319ecae6e7faf5f7a98f36f44f8d27274b3a3fd7d77f945891a7b68f9e2c028995f4da42e12995d76461caf7567fe90381af0191"
    "2fc83b4416188498def9d32d61fec9fc3d39e4b91133112975a8ddbe0d25ec126b798ce7569ef7f7c8f3931d793eded05fd2af925f84ea1aaf6a2155031cab87",
    "1240ef4eb811af875459e20561f56a87d3b81027f7f8902135af4aac10e2b46128d961a55ea7c8cf4335959534a083297f7179d95158553f7e21aef93a47f0f5"
    "1d882f93d6c6139ebfe10f1aff141aff74f5294cba02bf7eff2562fc0534dea4bfcc159a2bd5945579a0b2e20fcc0f420b15e3b5423c11aa1e878f4ebb215d96",
    "85ae3c0ff1a21012403724b2bac07643bc60fc12da48602a817a5b0f4ff7f0f164878f0f37af1bdcffc92bfedfe3d6c75336dfdfb9f159ea9b3f87dfa0ee7e0e"
    "260de4267dddad162bcaa25f4dcebbd153b1b448aee7fb7c67be277be6a3767e5af9ff25825fafd60d4a6f9f407c98b4317547ea0151eb4c5903d7379bafa0d5",
    "7ce16f986ffbd89d6f3bee35828f1a34ae66571f06f9b61c6bc5d8c0d3df353e0b2e9e262de55ce153f57eac1009837476b428c80d9ae069978fdf84f83069d3"
    "f4a92f80b4b67d2f348e1558951e78629fcad0f8b215bd98cfe72b2942cfb76284b662bc106dacc7cfffbb145c3c953299e4f8a4c468c251a3313eed75355001",
    "98d827afd6cd4b041f1f417c9834d0a489c8ab80d34d37f9303fd12e8e4ad0f892157d98df2f7d30e77b8a6ca82374470ee33bc6c63afcdb20dba54c64b460c6"
    "0320246bc951759c18d446753a152c1ca1d6ff07101f260db43e30acdcd00bfce4a1f1792b7a780b3f6bfe37e699e08742e3e7d7a9bb9f83494fc73cad4c392d",
    "d69c4564b9ac314791569bf2063fa87d4113e2d7a49dc6cfb33e5035bd0f50fb9b279bd7def0f3fe0a434ee0678e189f86c6a7ade8610f7ef6c559086ede1ebf"
    "cfee948f6be19bf2426acec7d791453e7b5d63baedac37b8f17bbdbc44bcffa17edb43f1c60d5995f86beb070ee3df357fcdab384219c1c7ef417c98b419cf32",
    "d661a72fab822c2b1d79caab7d419e753833c86f2f3e878aff7f0d8dfbdaae5e36f1b9fbe5b1135708769c4e4ecec7957eb11d1faa724aa50b7a63566c7b1457"
    "f82582df3f83f83569b7d7d1b36ffe87ce9017145eb5668fb6fe1fea733a202e08a41e3fcf493a16f69dd8b5c7e50f3e76dc59d93f19538becdc5d3fd2b1b8c5",
    "1d7f60cdb903718b40db39617edae6c3d7a5701d24c2b5d9cd117b229409de5cc51b901e2bde8044f0f666bc15bc4573c59132bae4e77ab6c80cca998b72262c"
    "2482b56f43e9edfb101f26bdcfcf2b0b13cd89b8e16bc4f80a34be62452f7be3a7b7626c6165675d5e7da2ff5b60719565b2cd26132935f40c5b1a5de72e8a83",
    "e82c132c5ca1e2219f417c98f43e5c158134d9d636bab12f43e1ad0acd57b5a2affbf1b612cf013b1668bc5d8bd1049d5854dabc3815f413fe321d894d3cb263"
    "b8d6d93ed49fd364557f5479e3cdf74f8fd5d9fe440a99fc3b80a3f7825c5f7b55494eae73d7a705968f7607f13a93e36a511a0fbbe5d5ba41d9addf87f83069",
    "d8be68890910f49c74b9ce2e3952cf84b253eee5d561716cd507667ff0abe0daa994008ed8f3d4207dd28d4dce7b25493c4d7815df5822f8f50a5f28bd7d0af1"
    "61d270bdad31e5ed7c6edbab0234be60452ffbeb556e3563232ff6e7ff1ce0f8c52011bdd44fa57a65a14983f27558617a740593f88557ebc64ade19c293244b",
    "0c900602afcb520a1c784ecbaebd6a40e31b8ee90712c7565dc7f2af831c1f8c5c5e35a615b67853caf553a2346b3022a77a64af705b3fa4be83d477b885333f"
    "eb3b96087e71b163bf0bf161d2901d63358d57f52b5600bd9519f3255e7805cd77e598beee8a67afae23d8768d51b5327bce4e5b42856deab3a6aedcd0538fea",
    "15715b3f6ec53bbcc61d897fe08f3b3fe31fb8e10eb5bffa36c48f491bbe96ac1ea8ff87e26b89982f07cd97b3a29f3b7943333ab492c8897cf33fd47f1c5c7b"
    "763e3e2bd5c6422a3a4a0c6b950ca38f53292e8d87ffe8d5ba21768cd8b120d8b125825f5cf0f6db101f26bdb76fc68bad55b3170f41e1aa0e8dab5bd1cb5e5c",
    "3d5fcbb1a323b22fb386abeef0b4dc9404bdc7c8223d91b2912e9dc8635237e515aebe46f0f187101f260dc74114455830ab45999e489c0e642927950596e377"
    "dee725e27dbe07bd8f49f737b37586acd413d6fdd8fe02314f179aa76b456ffbfdea7bc574a00e44fddf4870717822d5a528389985359691bb33be363a67531e",
    "c547705f4f6ed93daffd4c620ff1c7a19ff610b57e708b973c85f83169729e2cd8e75bfc3c4fb644f0eb555ecd01bb42fa1b52b7eb91f437c4b4bfe123c293a2"
    "ca3db37bed23c7d3560c5b78fade8fe9e0c611f54692290f6efae0a43fe98579ba7f9dbbac62629fbc5a37a4ae8ad455b9852f525745f045f045f0e5a73fb84f",
    "6f7bceb3e47a7367eaee517a71a48fe2bd7a31c4b0d747f1d97ffc2c1878dad7b7b7afa68b2dcdf82c6f52b3ab7a3c7dcac7e43c85079ebc5a37647f45f65741"
    "d85f79852707eaea271a5f6655c3794aca92a653f6e2e9af11e3dccb2bdc15c366fdfcf93ffe5130ecd45e5c757b29963b4f5412c91bb1b450d5303f3aa5dff5",
    "bcd4fef583d2df01fda36e2776b3fec9bdbe51b7ec3b51c7fba3d14f838babb3b15e99d1c3136d518b32f178799c9640cda37c2faaeea20ff16bd20edbab6740"
    "023a6005c3e7e94c794e97d5cefaa83fc963913cd67de3ade0cccf3c162afed586f83569a77126b223be339fc9eae8703d127c117c3d067ce16ec7483c9ec4e3",
    "ddc29d9ff178dc7167e5bc25d02456a2bc89cb3b7fbe605547684ae0c4be2cd0b8ea8bb9d344ecf45c1f66dbd1303bcc9e336a298d471cd1ab75e3d6f94ad217"
    "71f520e7297d3a4fe989ff83b05b5d41e6461d63110df8ceb60f2ff113899ff82efa8938e06d8d34459e1d7756d023f10f12ffb86fbc157ce11c5ffc9af2075f",
    "c49e117bf62eda33d4fec3bd73b610de28125f24387b7771865a475f42fc9ab43b766d65d26ef9227823780b22de1ecbbd463dd0ef7bb11f73ebfe0c937f07fa"
    "0004fa1ee7883aaf17c3b1935a691453c4cb712b9949d43dea67b344f0ebd5ba21f17b12bf770b5f38f743f40a5f28bd91f821891f3e86f8e112c1af57786a22",
    "f8f821c487494378e2cc3afd4bf87223ca997a0e947ffc1534fe2b2b7ada8baf7d62d9be472c307dd6f69dbb6c4ece4ab174862dd08d68362b36e8f0515ea182"
    "8537d4beeabd3b7cbc47894072caeef9da877eab0f431e27eaa4fee7432e1838da67b762178b5c539753b1a3ebd9f1225a3cbfba600698ecb3bc5a3728bd1d70",
    "1e4c62d50190b8e1c8cd38a06dfba40d5995ef85d6d66963a3deba4a6d2b896dfbf4e917d5e0eeaf16f3592ec6a6fb8368982bd132739d9e9ccd683ce28038ac"
    "a343eb7977ef4f797cf5bce4be14a7f044ee4b21f140120f740f5f7ec60351e750ded5fef16ee38ef48bc71f7741ba3fc5eafeeb8078843165492db2734bf9e6",
    "43fbc6bb177fdfb26fcb7ebdeaf4828ba36abc3414e2c719fe64d66c1d5d95eaaa58ba49e08123afd60d4a6f248e41e218ef521ce315c4af493b8dab6766a11c"
    "3fd7555e9c1c7a3f39c119c1d963c2196affc542fc9ab42b38eb0269176b87eaf3a1751a0eccf7a0fc17a9dbc0177738d76d78b56e5e22f8f808e2c3a48126ad",
    "436efa42e10fd4bfd3b872a41fabb93fed8339df5364838bd01db9ecf5c50e74fd6e26325a30e3011092b5e4a83a4e0c6aa33a8d495ed98fefe37d7c7c00f161"
    "d240eb0391d5877ee0c9b1ba953b785acb43eae1df8c477daefbea9da6639e56a69c166bce22b25cd698a348ab4d7983a7d7087eddcbefbcc1d3b33e5035bd0f",
    "0e89c793f35be4fc9655bb85f3b964af71f6d038baf95def86dd9a23e64b43f3a5ade8658fddb2d76f3e38f66a1f8ecac7b5f04d792135e7e3ebc8229fbdae31"
    "dd7696c4331ccc27f373252e083277f0e7e4f47de43568be9a637adc15cf6e5c91fa9b20e3109c71672762fd54612ad17a4a8b74afa431eb515c11b7f5833a9f",
    "f207103f26fd0db8bb94936bdff1d0cfe7a1f843f9952ef64f785bcc17fbcfe13cc0affc78f17e3070b877ff962d454f7bfa65839b25526717e9448e6e0d293c"
    "ece110e2d7a45db1876b37f38e5924f17d12df77d3fee1dcd7de6fdcb9554fecb51f4aea8bf1c721e93740eec924f7643a87279cefc9c4a53f0ec903903cc063",
    "cc03a0d6d163e9e3d637bcb843ecd47720794c5a900706bbc241f1ff2c34dea4eddfa761726f0b3ffff2133eb8f8614eb8ab49b358e94b47f255397b19c98c2e"
    "8e3caaff40ed171cb957f59ef5b2da6f99bf3e407f4ef79942e16989184fe219f8e28ad42b3a57afe8368e487d22be3822f589f6eb13ddc60fa947c4033fa41e",
    "d19d3a29b7f74569687cda8a1e485dd4bde3add81d3feba270db1791f81d89dfb985339ceb78718b3f1c705e79958f2d0b13cd89bc9327fad99fcfdd8ae1443f"
    "b6ab4ff4e0e69fb24cb6d96422a5869e614ba3ebdc4571109d79d4b706377c59b163fbf0c50c59854fdab76328fd34a0710d2bfab91f5f6fc4b0179ff85cf832",
    "b8f66bd8ae266303956e8d9bf1563bc75f31f1e3bc477e22aef927941ef7ed53219c09bc34d087943d7c2d117c5c42e34cda197cadd9b7b7ff5a3eff5621b8b8"
    "e2925cbb57495672f51618a7f469ac1e4dd4d3c1c6152a7e715f1f79b7eb8f48df787c71e467df78dccf77a1f449ee157a981f48f2bd78d6affb8d33b7ce51ba",
    "1ddf20e726f1c71d3937e9ddb949bb7823e7241f27eec839497fcf4912bf3218387b6c7ea5df3873eb5ca4db7e253907893feefc3c07896bfc91e4cf48fecc69"
    "9c91fcd9db38b3526f68acc8636dd2f5c45fbc80c69bb4fdbcfd4602dbfb322ad0f554f945bd358d9f0fbb553088c5867437d32ed01ee16989e0d7ab7543fc42",
    "e21712bf7063af9cc3d7b3cd6f9d95f79314584d23796992977e7c79695c71f412c1d77721be4c7a5d976eccd3317bb5f81ba72fdb5390a28229abf3a1bb2211"
    "7ff076bc159c814926536fb467a97455552ae23137eee56e30e98fe1d73a42f9879f417c99f4be38460d88bcb623afdbfe61151a5fa51c8d67acc471e0bc65a0",
    "ebedc51657889c96994c41c99ff552c5f1546e0cd378e4a3af207e4dda49bbc60a029054561af087e1ec77207e4cfa9efe82555e9095ad9cd89e1b3be41ebe3b"
    "12d93a37f65fb4145cbb5628a49367f1a29aef17444d2c0f87f1e69157f7c8fe12c1ef9f42fc9ab4dbebe8595f90cda8a3e94d763859d25920699ded3f6d9c4c",
    "ca31fbb77ab7a2f19ab0a397879eafdedee7ec9fdd7b23867dbb17ecfbd1a7e79193333e7f51eaa7627afb8cad169240cc7a83c79f23f89d43fc9ab4eb78045a"
    "47030389efdd05a1553fd1781f336c620b674bc438a7fb416efd037d276042f671d6f015cd1547cae8929febd9223328672eca99b0e091bdc3355e82b263bf05",
    "f165d2fbfccb1498821e7ff8e7e3745f6c069a8fb1a2bffbf7756bf1ecc75102bdaf3b4bb715edfab8db3f1f2c2eb5e9113768ccb21e9da3c6157f283dfe1ac4"
    "97499b41195137230d9d212f28bcba23a75b75208ee06b473fb2a285de92c37e3df1f09ffe3eb8f6ad3db8e8c7c10dcd9707293a56112bf9b096f1085f9e9c9f",
    "41af9f679c2c8ab2b4ee28b31b57742b9fedb59d23f96dfc71f8e8f2db98d66319881627ba37f73fb85357b391c0bedff87f7ff9f3e0da35fdb47193566a71be"
    "7d338b1fc52f227a432f7ab46f5b22f8f56adda0f4f609c48749af92775a673d19ab03597a2397dbf9ed0e34be43399f97bc239abdfae140c745d45aa1a7dd30",
    "8c0672fd8bd62899adc5db0d4ce2227ead2387ce5bb3aa19e697d55b79ddce6fd7a1f175ca29fff0f95a9ed04a9ed5371389f35bc35b77785a6e4a82de636491"
    "9e48d948974ee4318983f885b787d63fb28260d9af7c485f611a1a4f53f6f7ad06ef76f1f38b20e3a775a68eb9fc656f9a0073f6385a605bf249cfa37a2cbfd7",
    "8b5f75f9281ca1ec12895be08f2b3fe3167ee30aa5af03e278c67425352e2d5ccd2fbbd76760cbbead7aaac0f875fbfa7934b2f9a3929288b64add8bf38bd194"
    "9307f92885479cc28f7573f0b94b8d1f4fd8c3fc393ce37c1b09ece7af3efccf3ffed740e067affd49b5f2ad692ddacdc4691aa44ecf86cd7acea3f32c4b04bf",
    "5ead9b97083e3e86f8309feb4dd56aa60e27abbc17f1bd57d0f857940bfbc43722d9dc2f511ffe5580e3e7009412a962a4a0c8ed6efe88bbaef627ca1526e757"
    "fc5a47283d7e1fe2cba4217f4f62d50190b8e1c84edd054a3f5f41e3bea25ca8bbbc95c46e1fc5e5a75f5483bb7f5acc67b9189bee0fa261ae44cbcc757a7236",
    "a3f1b05f5ef97f4d041f3f84f8306908579c2c69faa52c31401a08bc2e4b29b0dfafb212a7701d6ff7ea699f58b6f116e8fd567372568aa5336c816e44b359b1"
    "41878ff20a8507debcca0ba3e281e45e3172af98557be6e7bd624b04bfb8d45d903ea40f5a8fa40fa94f7d485f23f875e75cf32d9e9e49b2687c2d8bbca413bb",
    "45ec16b15b5b7efdf20b497f0e5beb33d0e7b8fceccfb144f08b8b5f7840fc90dc33bb799de0caff7b6697087e1fbbdd72601ff7a0735ac48ee18f3762c79cb7"
    "635ee38cd835fc7146ec1ae9e346fab8b9872fbffab8fd3f918c9b9b", "" };

  nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(data, 96928U, &nameCaptureInfo);
  return nameCaptureInfo;
}

/* End of code generation (TransitiveKNN_part2.c) */
