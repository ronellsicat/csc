/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * TransitiveKNN_part2.h
 *
 * Code generation for function 'TransitiveKNN_part2'
 *
 */

#ifndef TRANSITIVEKNN_PART2_H
#define TRANSITIVEKNN_PART2_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "TransitiveKNN_part2_types.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern const volatile char_T *emlrtBreakCheckR2012bFlagVar;
extern emlrtContext emlrtContextGlobal;
extern emlrtRSInfo r_emlrtRSI;
extern emlrtRSInfo gb_emlrtRSI;

/* Function Declarations */
extern void TransitiveKNN_part2(const emlrtStack *sp, const emxArray_uint8_T *A,
  const emxArray_uint8_T *B, real_T k, real_T width, const emxArray_real_T
  *overSafeResults, const emxArray_real_T *errorMat, emxArray_real_T *CSH_ann,
  emxArray_real_T *numUniqueResultsPerPixel, emxArray_real_T *sortedErrors);
extern void TransitiveKNN_part2_api(const mxArray * const prhs[6], const mxArray
  *plhs[3]);
extern void TransitiveKNN_part2_atexit(void);
extern void TransitiveKNN_part2_initialize(void);
extern void TransitiveKNN_part2_terminate(void);
MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties(void);
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);

#endif

/* End of code generation (TransitiveKNN_part2.h) */
