function [similarity_matrix] = ComputeTileSimilarityMatrix(image_codes, tile_size, plots_dir)

num_images = size(image_codes, 1)
num_tables = size(image_codes, 2)

%% image_codes contain all the patch projections to T codes/tables/low-dim-spaces.
% image_codes{I, T} is an H x W matrix same size as the image where each
% uint32 value image_codes{I, T}(i, j) is the binary coordinate of patch
% i,j of image I in subspace/projection/table T.

%% TODO: Implement multidim map for counting similar patches across tiles.
% See https://www.mathworks.com/matlabcentral/fileexchange/33068-a-multidimensional-map-class
% for possible library.

%% Create an empty matrix with similarity 0 in the beginning. The size of
% the matrix depends on the number of images and tile size.

matrix_size = 0;
num_tiles_per_image = {};
max_num_tiles_per_image = [0, 0];

% Compute size of matrix:
for image_index = 1 : num_images
   
    % height / tilesize, width / tilesize
    img_height = size(image_codes{image_index, 1}, 1)
    img_width = size(image_codes{image_index, 1}, 2)
    num_tiles_per_image{1, image_index} = [ceil( img_height/ tile_size), ... 
        ceil(img_width / tile_size)]
    matrix_size = matrix_size + (num_tiles_per_image{1, image_index}(1,1) * ...
        num_tiles_per_image{1, image_index}(1,2));
    
    max_num_tiles_per_image = [max(max_num_tiles_per_image(1,1), num_tiles_per_image{1, image_index}(1,1)), ...
        max(max_num_tiles_per_image(1,1), num_tiles_per_image{1, image_index}(1,2))];
end

%% Compute feature histogram for each tile:

debug = 0;
feature_histogram = cell(num_images, max_num_tiles_per_image(1,1), max_num_tiles_per_image(1, 2));
num_patches_per_image_per_tile = cell(num_images, max_num_tiles_per_image(1,1), max_num_tiles_per_image(1, 2));

for image_index = 1 : num_images
   
    for tile_index_row = 1 : num_tiles_per_image{1, image_index}(1, 1)
            for tile_index_col = 1 : num_tiles_per_image{1, image_index}(1, 2)
                
                img_height = size(image_codes{image_index, 1}, 1);
                img_width = size(image_codes{image_index, 1}, 2);
    
                upper_left_coord_row = min(((tile_index_row-1) * (tile_size)) + 1, img_height);
                upper_left_coord_col = min(((tile_index_col-1) * (tile_size)) + 1, img_width);
               
                lower_right_coord_row = min(upper_left_coord_row + tile_size - 1, img_height);
                lower_right_coord_col = min(upper_left_coord_col + tile_size - 1, img_width);
          
                
                num_patches_per_image_per_tile{image_index, tile_index_row, tile_index_col} = ...
                    (lower_right_coord_row - upper_left_coord_row + 1) * (lower_right_coord_col - upper_left_coord_col + 1);
                
                % Debug write down the tile image:
                if(debug == 1)
                    for tt = 1 : num_tables
                        tile_codes = image_codes{image_index, tt}(upper_left_coord_row:lower_right_coord_row, ...
                            upper_left_coord_col:lower_right_coord_col);
                        WriteCodesToTiff(tile_codes, sprintf('./tile_im%d_t%d_r%d_c%d', image_index, tt, tile_index_row, tile_index_col));
                    end
                end
                
                cur_histogram = containers.Map;
                
                % Do the counting
                for patch_row = upper_left_coord_row : lower_right_coord_row
                    for patch_col = upper_left_coord_col : lower_right_coord_col
                       
                        map_key = GenerateFeaturesKeyString(image_codes, image_index, patch_row, patch_col);
                        
                        AddCount(cur_histogram, map_key);                       
                    end
                end
                
                % DEBUG:
                %num_hist_keys = size(cur_histogram.keys)
                
                feature_histogram{image_index, tile_index_row, tile_index_col} = cur_histogram; 
            end
    end

    if(debug == 1)
        % DEBUG:
        WriteCodesToTiff(image_codes{image_index, 1}, sprintf('./im%d', image_index));
    end
end

%DisplayFeatureHistogram(feature_histogram, num_images, num_tiles_per_image);

% Compute histogram similarities:
similarity_matrix = ComputeHistogramSimilarities(feature_histogram, num_images, num_tiles_per_image, num_patches_per_image_per_tile, matrix_size, plots_dir);

%figure();
%imgesc(similarity_matrix);
%colorbar

end



function [similarity_matrix] = ComputeHistogramSimilarities(feature_histogram, num_images, num_tiles_per_image, num_patches_per_image_per_tile, matrix_size, plots_dir)

    similarity_matrix = zeros(matrix_size);
    tile_names = cell(1, matrix_size);

    hist1_index = 1;
    for image_index = 1 : num_images
        for tile_index_row = 1 : num_tiles_per_image{1, image_index}(1, 1)
            for tile_index_col = 1 : num_tiles_per_image{1, image_index}(1, 2)
                
                tile_names{1, hist1_index} = sprintf('im%d_r%d_c%d', image_index, tile_index_row, tile_index_col);
                hist1 = feature_histogram{image_index, tile_index_row, tile_index_col};
                num_patches1 = num_patches_per_image_per_tile{image_index, tile_index_row, tile_index_col};
                
                hist2_index = 1;
                for image_index2 = 1 : num_images
                    for tile_index_row2 = 1 : num_tiles_per_image{1, image_index}(1, 1)
                        for tile_index_col2 = 1 : num_tiles_per_image{1, image_index}(1, 2)
                            
                            if(similarity_matrix(hist1_index, hist2_index) > 0) 
                                similarity_matrix(hist2_index, hist1_index) = similarity_matrix(hist1_index, hist2_index);
                            else
                                hist2 = feature_histogram{image_index2, tile_index_row2, tile_index_col2};
                                num_patches2 = num_patches_per_image_per_tile{image_index2, tile_index_row2, tile_index_col2};
                            
                                similarity_matrix(hist1_index, hist2_index) = ComputeHistogramSimilarity(hist1, num_patches1, hist2, num_patches2);
                            end
                            
                            hist2_index = hist2_index + 1;
                        end
                    end
                end
                
                
                hist1_index = hist1_index + 1;
            end
        end
    end

%    figure('units','normalized','outerposition',[0 0 1 1])
    figure();
    imagesc(similarity_matrix);
    title('Appearance Similarity Matrix');
    colorbar
    colormap gray;
    caxis([0, 1]);
    xticks([1:1:size(similarity_matrix,2)]);
    xticklabels([1:1:size(similarity_matrix,2)]);
    yticks([1:1:size(similarity_matrix,1)]);
    yticklabels([1:1:size(similarity_matrix,1)]);
    saveas(gcf,sprintf('%splots_appearance_similarity_matrix.png', plots_dir));
    
    
    colormap parula;
    saveas(gcf,sprintf('%splots_appearance_similarity_matrix_parula.png', plots_dir));
    f = figure(); %'units','normalized','outerposition',[0 0 1 1])
    t = uitable(f,'Data',similarity_matrix);
    t.ColumnName = tile_names;
    set(t,'Position',[20 20 matrix_size*60 matrix_size*40]);
    writetable(table(similarity_matrix), 'plots_similarity.csv');
    drawnow;
    saveas(gcf,sprintf('%splots_appearance_similarity_table.fig', plots_dir));
end


function similarity_measure = ComputeHistogramSimilarity(hist1, num_patches1, hist2, num_patches2)
    
    % Compute histogram intersection:
    keys1 = hist1.keys;
    numpatches_intersecting = 0;
    num_entries_in_1 = 0;
    for k = 1 : size(keys1, 2)
       
        % If the key exists in hist2, count the minimum patches in the two
        % bins:
        cur_key = keys1{1, k};
        if(hist2.isKey(cur_key)) 
            
            numpatches_intersecting = numpatches_intersecting + min(hist1(cur_key), hist2(cur_key)); 
        end
       
        num_entries_in_1 = num_entries_in_1 + hist1(cur_key);
    end
    
    % DEBUGGING:
    debug = false;
    if(debug)
            num_entries_in_2 = 0;
        keys2 = hist2.keys;
        for kk = 1 : size(keys2, 2)

            % If the key exists in hist2, count the minimum patches in the two
            % bins:
            cur_key = keys2{1, kk};

            num_entries_in_2 = num_entries_in_2 + hist2(cur_key);
        end

        num_patches1
        num_patches2
        numpatches_intersecting
        num_entries_in_1
        num_entries_in_2
        % END DEBUGGING.
    end

    
    similarity_measure = (numpatches_intersecting * 2) / (num_patches1 + num_patches2);
    
end



function [] = DisplayFeatureHistogram(feature_histogram, num_images, num_tiles_per_image)

    for image_index = 1 : num_images
   
        for tile_index_row = 1 : num_tiles_per_image{1, image_index}(1, 1)
            for tile_index_col = 1 : num_tiles_per_image{1, image_index}(1, 2)
                
                cur_histogram = feature_histogram{image_index, tile_index_row, tile_index_col};
                hist_keys = keys(cur_histogram);
                
                
                
                for kk = 1 : cur_histogram.Count
                    fprintf('key %s has %d entries.\n', hist_keys{1, kk}, cur_histogram(hist_keys{1, kk}));
                end
            end
        end
    end
end


function [cur_histogram] = AddCount(cur_histogram, map_key)

         if(cur_histogram.isKey(map_key))
            cur_histogram(map_key) = cur_histogram(map_key) + 1;

        else
            % If map_key does not exist, start it with count 1.
            cur_histogram(map_key) = 1;
        end
end

function [] = WriteCodesToTiff(data, filename) 

        t = Tiff(sprintf('./%s.tif', filename),'w');
        % Setup tags
        % Lots of info here:
        % http://www.mathworks.com/help/matlab/ref/tiffclass.html
        tagstruct.ImageLength     = size(data,1);
        tagstruct.ImageWidth      = size(data,2);
        tagstruct.Photometric     = Tiff.Photometric.MinIsBlack;
        tagstruct.BitsPerSample   = 32;
        tagstruct.SamplesPerPixel = 1;
        tagstruct.RowsPerStrip    = 16;
        tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
        tagstruct.Software        = 'MATLAB';
        t.setTag(tagstruct);
        t.write(data);
        t.close();
        
        d = imread(sprintf('./%s.tif', filename));
        disp(class(d));
        assert(isequal(d,data));   
end


function map_key = GenerateFeaturesKeyString(image_codes, image_index, patch_row, patch_col)

    num_tables = size(image_codes, 2);
    
    map_key = num2str(image_codes{image_index, 1}(patch_row, patch_col));   % Get the first entry
    for table_index = 2 : num_tables
       
        map_key = strcat(map_key, sprintf(',%d', image_codes{image_index, table_index}(patch_row, patch_col)));
    end
end