function [image_codes] = ComputeImageCodes(images_fnames, patch_size, num_tables)

AddPaths

fprintf('Compute patch similarity codes!!!\r\n');
fprintf('*******************************\r\n');

%% DESCRIPTION: This code takes in a list of images and computes T projections 
% of each of the image patches. The projections are to different low
% dimensional spaces described by shifted Walsh-Hadamard kernels i.e. each
% patch is projected to a low-dimensional space described by the selection
% of shifted WH kernels. The output of this method is for each image, T
% images for which each entry t(i,j) is the integer representation of the
% binary coordinate of the patch (i,j) for table t's low dimensional space.
% USAGE: For a patch p(x,y), if we want to find similar patches, we need to
% look for all patches that satisfy pT_t(x,y) == pT_t(i,j) for all tables
% i.e. similar patches will be mapped to the same point in all T tables.


%% PARAMS:
%images_dir = './test/';
%num_tables = 2;     % this is therefore the number of lower dimensional spaces.
%patch_size = 8;     % only allowed sizes are [2,4,8,16,32]


%% Load the images

num_images = length(images_fnames);
images = cell(1, num_images);
for ff = 1 : length(images_fnames) % Here we start with index 3 because 1 and 2 correspond to dirs: ., and ..
    
    images{1,ff} = imread(images_fnames{1,ff});
end


%% Initialize random seed (this is useful mainly for debugging)
rng(12345689);

%% Example: run CSH with user defined parameters settings
%close all

k = 1;
fprintf('-------> Runing CSH_nn example: patch_size = %d, num_tables = %d, k = %d\r\n',patch_size,num_tables,k);
CSH_TIC_A2B = tic;
%%%%%% CSH RUN %%%%%%%
image_codes = CSH_nn_similarity(images,patch_size,num_tables,k,0);
%%%%%%%%%%%%%%%%%%%%%%
CSH_TOC = toc(CSH_TIC_A2B);
fprintf('   CSH_nn elapsed time: %.3f[s]\r\n' , CSH_TOC);


%% image_codes contain all the patch projections to T codes/tables/low-dim-spaces.
% image_codes{I, T} is an H x W matrix same size as the image where each
% uint32 value image_codes{I, T}(i, j) is the binary coordinate of patch
% i,j of image I in subspace/projection/table T.

%% TODO: Implement multidim map for counting similar patches across tiles.
% See https://www.mathworks.com/matlabcentral/fileexchange/33068-a-multidimensional-map-class
% for possible library.


return

save('./image_codes.mat', 'image_codes'); 

return

for ii = 1 : num_images

    for tt = 1 : num_tables
    
        data = image_codes{ii,tt};
        t = Tiff(sprintf('./im_%d_table_%d.tif', ii, tt),'w');
        % Setup tags
        % Lots of info here:
        % http://www.mathworks.com/help/matlab/ref/tiffclass.html
        tagstruct.ImageLength     = size(data,1);
        tagstruct.ImageWidth      = size(data,2);
        tagstruct.Photometric     = Tiff.Photometric.MinIsBlack;
        tagstruct.BitsPerSample   = 32;
        tagstruct.SamplesPerPixel = 1;
        tagstruct.RowsPerStrip    = 16;
        tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
        tagstruct.Software        = 'MATLAB';
        t.setTag(tagstruct)
        t.write(data);
        t.close();
    end
end


end