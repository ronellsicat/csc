function expval_pyramid = SHM_ComputeExpectedValuePyramid(pdf_pyramid)

    pyramid_size = size(pdf_pyramid);
    num_levels = pyramid_size(1);

    expval_pyramid = cell(pyramid_size);
    
    for level = 1 : num_levels
        
        pdf_image = pdf_pyramid{level, 1};
        size(pdf_image)
        expval_pyramid{level, 1} = SHM_ComputeExpectedValueImage(pdf_image);
    end
   
end