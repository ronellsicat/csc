function output_images = SHM_ComputeMipmap( input_image )
% compute full mipmap hierarchy for input image (i.e., all mipmap levels)

    display( 'SHM_ComputeMipmap' );

    % size of full-resolution image
    img_size = size( input_image );

    % compute number of levels down to a single pixel
    num_levels_xy = ceil( log2( img_size ) + 1 );
    num_levels = max( num_levels_xy( 1 ), num_levels_xy( 2 ) )

    % store all mipmap levels in cell array
    output_images = cell( num_levels );
    
    % store original image as first level
    output_images( 1 ) = { input_image };

    % compute all other levels
    for level = 2 : num_levels

        % compute next level
        output_images( level ) = { SHM_ComputeMipmapLevel( output_images{ level - 1 } ) };
        
        % display level
%       figure, imshow( output_images{ level } ), title( 'mipmap level' );
        
    end

end
