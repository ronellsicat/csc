% Computes a list of pdf images for all the png images in
% image_db_base_dir. Output pdf images can be indexed as:
% pdf_images{image_index, 1}(b, r, c).
%
% If the input images are 1D, the output 2D pdf images are
% visualized as scaled images.

function pdf_images = SHM_ComputePDFImages(image_db_base_dir, ...
    target_level, pdf_num_bins, ...
    smooth_kernel_size_rc, smooth_kernel_sigma_rc, ...
    smooth_kernel_size_b, smooth_kernel_sigma_b, show_expval_images, ...
    show_1D_pdf_images, output_base_dir)

% Initial set up:
image_fnames = dir(image_db_base_dir);  % Get all filenames in directory.
image_fnames = image_fnames(3:end);     % Remove . and .. subdirectories from list.
num_images = length(image_fnames);
pdf_images = cell(num_images, 1);

if ~exist(output_base_dir, 'dir')
    mkdir(output_base_dir);        
end
        
expval_images_dir = strcat(output_base_dir, 'expval/');
if(strcmp(show_expval_images, 'yes') ~= 0)
    if ~exist(expval_images_dir, 'dir')
        mkdir(expval_images_dir);
    end
end

% Set up smoothing kernels:
spatial_smoothing_kernel = SHM_ComputeGaussianKernel(smooth_kernel_size_rc, smooth_kernel_sigma_rc)
range_smoothing_kernel = SHM_ComputeGaussianKernel(smooth_kernel_size_b, smooth_kernel_sigma_b)

% For each image, compute pdf hierarchy up to target level.
for i = 1 : num_images
    
    input_image_fname = sprintf('%s%s', image_db_base_dir, image_fnames(i).name);
    pdf_pyramid = SHM_ComputePDFPyramid(input_image_fname, target_level, pdf_num_bins, ...
                spatial_smoothing_kernel, range_smoothing_kernel);
            
    pdf_images{i, 1} = pdf_pyramid{target_level, 1};
    
    if(strcmp(show_expval_images, 'yes') ~= 0)
        expval_image = SHM_ComputeExpectedValueImage(pdf_images{i, 1});
        figure
        imshow(uint8(expval_image));
        imwrite(uint8(expval_image), sprintf('%sexpval_%d_level_%d.png', ...
            expval_images_dir, i, target_level));
    end
end

% Visualize pdf images for 1D input images.
pdf_image_size = size(pdf_images{1, 1})
if (pdf_image_size(2) == 1) && (strcmp(show_1D_pdf_images, 'yes') ~= 0)
    display('Visualizing 1D pdf images.');
    for i = 1 : num_images
        figure
        imagesc(reshape(pdf_images{i, 1}, [pdf_image_size(1), pdf_image_size(3)]));
        colorbar
    end
end

end