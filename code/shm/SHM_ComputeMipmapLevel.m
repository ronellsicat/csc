function output_image = SHM_ComputeMipmapLevel( input_image )
% compute single mipmap level, reduced by one step from input image

    display( 'SHM_ComputeMipmapLevel' );

    % size of previous level
    img_size = size( input_image );

    % compute size of reduced output mipmap level,
    % take possible rectangular shape into account
    if ( img_size( 2 ) > 1 )
        newsizex = img_size( 2 ) / 2;
    else
        newsizex = 1;
    end
    
    if ( img_size( 1 ) > 1 )
        newsizey = img_size( 1 ) / 2;
    else
        newsizey = 1;
    end
    
    % produce downsampled image of half the size in each axis
    for yindx = 0 : newsizey - 1
        for xindx = 0 : newsizex - 1

            if ( xindx * 2 + 1 < img_size( 2 ) )
                ofsxr = 2;
            else
                ofsxr = 1;
            end
                
            if ( yindx * 2 + 1 < img_size( 1 ) )
                ofsyr = 2;
            else
                ofsyr = 1;
            end
            
            % compute downsampled pixel with averaging
            output_image( yindx + 1, xindx + 1 ) = ...
                0.25 * input_image( yindx * 2 + 1,     xindx * 2 + 1 ) + ...
                0.25 * input_image( yindx * 2 + 1,     xindx * 2 + ofsxr ) + ...
                0.25 * input_image( yindx * 2 + ofsyr, xindx * 2 + 1 ) + ...
                0.25 * input_image( yindx * 2 + ofsyr, xindx * 2 + ofsxr );
        end
    end

end
