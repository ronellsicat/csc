%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%>
% Generate separate images from an input images by cropping.
% Output are non-overlapping.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%>
% @copybrief split_folders_files.m
%
% @param input_filename image filename
% @param out_width cropping width
% @param out_height cropping height
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [num_files, orig_size] = generate_image_crops(input_filename, out_base_dir, out_width, out_height)

I = imread(input_filename);
Isize = size(I);
orig_size = Isize;

num_crops_c = floor(Isize(1) / out_width);
num_crops_r = floor(Isize(2) / out_height);
num_files = 1;

for r = 1 : num_crops_r
   for c = 1 : num_crops_c
      
       row_start = ((r-1) * out_height) + 1;
       row_end = row_start + out_height - 1;
       col_start = ((c-1) * out_width) + 1;
       col_end = col_start + out_width - 1;
       
       cropped_image = I(row_start:row_end, col_start:col_end, :);
       imwrite(cropped_image, sprintf('%s%d.png', out_base_dir, num_files));
       num_files = num_files + 1;
   end
end

end