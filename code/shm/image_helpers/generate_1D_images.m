% Generate 1D images from input by slicing along the rows.

function out = generate_1D_images(input_filename, out_base_dir, num_images)

I = imread(input_filename);
Isize = size(I);

num_lines_to_skip = floor(Isize(2) / num_images);

image_width = Isize(2);
cur_row = 1;

for image_num = 1 : num_images

    cropped_image = I(cur_row:cur_row, 1:image_width, :);
    imwrite(cropped_image, sprintf('%s%d.png',out_base_dir, image_num));
    cur_row = cur_row + num_lines_to_skip;
end

out = image_num;
end