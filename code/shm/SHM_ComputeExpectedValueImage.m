function expval_image = SHM_ComputeExpectedValueImage(pdf_image)

pdf_image_size = size(pdf_image);
range_res = pdf_image_size(1);
r_res = pdf_image_size(2);
c_res = pdf_image_size(3);

binning_scale = 256 / range_res;

expval_image = zeros(r_res, c_res);

for r = 1 : r_res
    for c = 1 : c_res
        for b = 1 : range_res
            
            expval_image(r, c) = expval_image(r, c) + (single(b - 1) * binning_scale * pdf_image(b,r,c));
        end
    end
end
end