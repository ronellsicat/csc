function [ output ] = SHM_DownSampleImage( input, r_rate, c_rate )

input_size = size(input);
if(input_size(1) == 1)
    
    output = downsample(input, c_rate);
    return
end

if(input_size(2) == 1)
    
    output = downsample(input', r_rate)';
    return
end

output = downsample(downsample(input', r_rate)', c_rate);

end

