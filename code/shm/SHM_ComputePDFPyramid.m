function pdf_pyramid = SHM_ComputePDFPyramid(image_filename, num_levels, num_bins, ...
                spatial_smoothing_kernel, range_smoothing_kernel)

	downsampling_rate = 2;
    display( 'SHM_ComputePDFPyramid' );
            
    image_mat = double(imread(image_filename));
    image_size = size(image_mat);
    
    pdf_pyramid = cell(num_levels, 1);
    pdf_pyramid{1, 1} = double(zeros(horzcat(num_bins, image_size)));
    
    binning_scale_factor = num_bins / 256;
    
    % compute first pdf level (binary pdf convolved with range_smoothing kernel):
    for r = 1 : image_size(1)
        for c = 1 : image_size(2)
        
            pdf = pdf_pyramid{1, 1}(:,r,c);
            
            % the +1 in the end is for proper indexing in matlab
            pixel_bin = floor(image_mat(r, c) * binning_scale_factor) + 1;       
            pdf(pixel_bin) = 1.0;
            
            % convolve binary pdf with range smoothing kernel
            pdf = imfilter(pdf, range_smoothing_kernel);
            
            % normalize pdf so it sums to 1
            pdf = pdf / norm(pdf, 1);
            
            pdf_pyramid{1, 1}(:,r,c) = pdf;
        end
    end
   
    if(num_levels <= 1) 
        return; 
    end
    
    % compute pdf hierarchy (iteratively smooth/filter the pdf and
    % down-sample):
    for level = 2 : num_levels
    
        per_bin_res = image_size;
        image_size = ceil(image_size / 2);
        pdf_pyramid{level, 1} = double(zeros(horzcat(num_bins, image_size)));
        
        
        % apply spatial filter and down-sample per bin:
        for bin = 1 : num_bins
            smoothed_bin_image = reshape(imfilter(pdf_pyramid{level - 1, 1}(bin, :, :), spatial_smoothing_kernel), ...
                per_bin_res);
            pdf_pyramid{level, 1}(bin,:,:) = SHM_DownSampleImage(smoothed_bin_image, downsampling_rate, downsampling_rate);
        end
        
        % normalize pdfs so each pdf sums to 1:
        for r = 1 : image_size(1)
            for c = 1 : image_size(2)

                pdf = pdf_pyramid{level, 1}(:,r,c);
                pdf = pdf / norm(pdf, 1);

                pdf_pyramid{level, 1}(:,r,c) = pdf;
            end
        end
    
    end
    
end