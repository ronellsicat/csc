function kernel = SHM_ComputeGaussianKernel(size, sigma)

    % returns an isotropic Gaussian kernel
    % kernel is the impulse response of the imgaussfilt function.
    
    temp = zeros(size);
    temp(ceil(size(1)/2), ceil(size(2)/2)) = 1;
    kernel = imgaussfilt(temp, sigma);

end