%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

clear all;
close all;

addpath('./shm');
addpath('./shm/image_helpers');
addpath('./sparse_coding');
addpath('./sparse_coding/image_helpers');



%% NOTES:
% 1. We follow Matlab's convention for image indexing i.e. an image is a
% matrix with width = number of cols, and height = number of rows. Thus,
% we index and image with r,c: r goes through the rows while c goes through
% the columns.
%
% 2. We refer to the range domain as bin and use b for indexing.

%% EXPERIMENT 1:
% This experiment prepares a database of 2D images and uses CSC to learn a
% dictionary which can then be used to reconstruct a noisy image.
%
% Here, we prepare a learning image database of 2D images by 
% chopping up a hi-res image. If you already have
% a database of 2D images for learning and for testing, set skip_setup = 1 
% (otherwise set skip_setup = 0).
%
skip_setup = 0;

%
%% 1. Chop a hi-res 8-bits/pixel image into smaller 128 x 128 images.
%
% Set params:
%
full_image_fname = '../data/test_images/barbara.png';
image_db_base_dir = '../data/image_dbs/barbara_128_128/';

%
if ~skip_setup
    
    if ~exist(image_db_base_dir, 'dir')
        mkdir(image_db_base_dir);        
    end
    
    generate_image_crops(full_image_fname, image_db_base_dir, 128, 128);
end

%% 2. Remove random images from the image db to be later used for
% reconstruction experiments.
%
% Set params:
%
test_images_base_dir = '../experiments/exp_1/test_images/';
test_indices = [4];
%test_indices = [4, 10];
%
image_db_fnames = dir(image_db_base_dir);
image_db_fnames = image_db_fnames(3:end);
%
if ~skip_setup
    
    if ~exist(test_images_base_dir, 'dir')
        mkdir(test_images_base_dir);        
    end
    
    for i = 1 : length(test_indices)
        movefile(sprintf('%s%s', image_db_base_dir, image_db_fnames(test_indices(i)).name), ...
           sprintf('%s%s', test_images_base_dir, image_db_fnames(test_indices(i)).name));
    end
else
    fprintf('Skipping set-up. Using images in %s.\n\n', image_db_base_dir);
end

%% 3. Use Felix's code to learn dictionary (with specified number of
% kernels). If learning was done previously with output saved to a mat
% file, you can skip learning by setting skip_learning = 1.
% (otherwise set skip_learning = 0)
%
skip_learning = 0;

% Set params:
%
output_base_dir = '../experiments/exp_1/';
kernel_size_rc = [11, 11];
num_kernels = 12;
lambda_residual = 1.0;
lambda = 1.0;
save_iteration_imgs = 1;
%
if ~skip_learning
    [d, z, Dz, obj, iterations, out_fname] = learn_kernels_2D(image_db_base_dir, ...
        output_base_dir, save_iteration_imgs, kernel_size_rc, num_kernels, lambda_residual, lambda);
end

%% 4. Using learned dictionary from previous step, perform reconstruction 
% (on noisy and non-noisy signal to simulate compression).
%
% Set params:
%
pct_missing_pixels = 2;   % Set this to 2 so that we don't have any missing pixels.
filters_fname_on_disk = '../experiments/exp_1/filters_ours_obj2.28e+04.mat';
%
if ~skip_learning
    filters_filename = out_fname;
else
    fprintf('Skipped dictionary learning. Using dictionary in %s.\n\n', filters_fname_on_disk);
    filters_filename = filters_fname_on_disk;    
end
%
[feature_maps, reconst_image] = reconstruct_LMM_2D_sparse(filters_filename, test_images_base_dir, ...
    output_base_dir, pct_missing_pixels);

% Display feature maps used in reconstruction and save to disk (for visualization only):
%
plot_feature_maps_sum = 'yes';
feature_maps_sum = compute_feature_maps_sum(feature_maps, plot_feature_maps_sum);
print(sprintf('%sfmaps_sum_fig', output_base_dir), '-dpng')
imwrite(feature_maps_sum, sprintf('%sfmaps_sum.png', output_base_dir));

% Copy code to directory:
copyfile(sprintf('%s.m', mfilename('fullpath')), sprintf('%s', image_db_base_dir));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
