%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

clear all;
close all;

addpath('./shm');
addpath('./helper_functions');
addpath('./shm/image_helpers');
addpath('./sparse_coding');
addpath('./sparse_coding/image_helpers');


%% NOTES:
% 1. We follow Matlab's convention for image indexing i.e. an image is a
% matrix with width = number of cols, and height = number of rows. Thus,
% we index and image with r,c: r goes through the rows while c goes through
% the columns.

%% EXPERIMENT 4:
% This experiment uses CSC to learn a dictionary from one or more images 
% inside a single directory. The outputs are stored in a data.mat file
% consisting of d = dictionary, z = feature maps, ...
%
% This tries to answer Question 1 in: https://docs.google.com/document/d/1MoG3cx3QKQjMSQMDgP7A-V9E_FkglKuTFfX47uYI9zQ/edit

%% PARAMETERS:
%
% CSC parameters:
%
keyword = 'fruits100';                                 % This keyword can be used to uniquely identify an experiment.
skip_learning = 1;                                      % Set to 0 to perform training; 1 to load data.mat from output dir.
image_db_base_dir = '../data/image_dbs/fruit_100_100/';       % Dir with input image/s for learning.
output_root_dir = '../experiments/exp_5/';              % Dir where to write output.
kernel_size_rc = [11, 11];                              % Size of dictionary atoms.
num_kernels = 9;                                       % Number of dictionary atoms.
lambda_residual = 1.0;
lambda = 1.0;                                          % Sparsity: the higher this is the more sparse the feature maps will be.
save_iteration_imgs = 1;                                % Set to 1 to save learning iteration images.

% Parameters for splitting an image into smaller tiles.
%
learn_from_tiled_image = false;
image_to_tile = '../data/image_dbs/barbara512/1.png';
tile_size = [128 128];



%% INITIALIZATIONS:
%
% Create output dirs:
output_base_dir = strcat(output_root_dir, sprintf('%s_d%d_%dx%d_lr%f_l%f/', keyword, num_kernels, ...
    kernel_size_rc(1), kernel_size_rc(2), lambda_residual, lambda));

if ~exist(output_root_dir, 'dir')
        mkdir(output_root_dir);        
end

if ~exist(output_base_dir, 'dir')
        mkdir(output_base_dir);        
end

copy_of_input_base_dir = sprintf('%s/input', output_base_dir); 
if ~exist(copy_of_input_base_dir, 'dir')
        mkdir(copy_of_input_base_dir);        
end

input_image_size = [0, 0];
if(learn_from_tiled_image)
    [num_tiles, input_image_size] = generate_image_crops(image_to_tile, image_db_base_dir, tile_size(1, 1), tile_size(1, 2));
else
    % Copy input images to output dir:
    image_db_fnames = dir(image_db_base_dir);
    for i = 3 : length(image_db_fnames) % Here we start with index 3 because 1 and 2 correspond to dirs: ., and ..

        copyfile(sprintf('%s/%s', image_db_fnames(i).folder, image_db_fnames(i).name), ...
            sprintf('%s/%s', copy_of_input_base_dir, image_db_fnames(i).name));
    end
end


%% ADMM LEARNING OR LOAD PRECOMPUTED DATA:
%
out_fname = sprintf('%s/data.mat', output_base_dir);
if ~skip_learning
    [dictionary, fmaps, reconst, obj, iterations, orig_imgs, image_names] = learn_kernels_2D(image_db_base_dir, ...
        output_base_dir, save_iteration_imgs, kernel_size_rc, num_kernels, lambda_residual, lambda);
    
    image_names = arrange_image_names_by_number(image_names)
    
    % Save learning output to file:
    save(out_fname, 'dictionary', 'fmaps', 'reconst', 'obj', 'iterations', 'orig_imgs', 'image_names');

else 
    % Load data from file:
    load(out_fname, 'dictionary');
    load(out_fname, 'fmaps');
    load(out_fname, 'reconst');
    load(out_fname, 'orig_imgs');
    load(out_fname, 'obj');
    load(out_fname, 'iterations');
    load(out_fname, 'image_names');
    image_names = arrange_image_names_by_number(image_names)
end

% If a single image was split into tiles for learning, concatenate the 
% results together as if everything was done on one image just to make the
% analysis work like for single images which is better for visualization.
out_fname_cat = sprintf('%s/data_cat.mat', output_base_dir);
if(learn_from_tiled_image)
 
    % Transform data:   
    atom_size = [size(dictionary, 1), size(dictionary,2)];
    image_boundary = [floor(atom_size(1)/2), floor(atom_size(2)/2)];
    
    fmaps = ConcatenateData(fmaps, input_image_size, tile_size, image_boundary, image_names);
    reconst = ConcatenateData(reconst, input_image_size, tile_size, image_boundary, image_names);
    orig_imgs = ConcatenateData(orig_imgs, input_image_size, tile_size, [0,0], image_names);
    image_names = arrange_image_names_by_number(image_names)

    % Save:
    save(out_fname_cat, 'dictionary', 'fmaps', 'reconst', 'obj', 'iterations', 'orig_imgs', 'image_names');
end

%% ANALYSIS / VISUALIZATIONS:
%
% Start interactive visualization ui.

if(learn_from_tiled_image)
    interactive({out_fname_cat}); 
else
    interactive({out_fname});     
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%