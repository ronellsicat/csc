%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

clear all
close all

addpath('./shm');
addpath('./sparse_coding');

% NOTES:
% 1. We follow Matlab's convention for image indexing i.e. an image is a
% matrix with width = number of cols, and height = number of rows. Thus,
% we index and image with r,c: r goes through the rows while c goes through
% the columns.
%
% 2. We refer to the range domain as bin and use b for indexing.

%% EXPERIMENT 1:
% Prepare a learning image database of 1D images (2D pdf images) by 
% preprocessing a hi-res image. Steps 1- 3 are unnecessary if you already have
% a database of 1D images.
%
% 1. Chop a hi-res image into smaller 128 x 128 images.
%
% Set params:
%
full_image_fname = '../data/test_images/barbara.png';
image_db_base_dir_2D_crops = '../data/image_dbs/barbara_128_128/';
%
generate_image_crops(full_image_fname, image_db_base_dir_2D_crops, 128, 128);

% 2. Take 1D slices (along r) of a single image.
%
% Set params:
%
test_images_base_dir = '../data/test_images/barbara_128_1/';
image_db_base_dir = '../data/image_dbs/barbara_128_1/';
image_index_to_slice = 1;
num_slices = 16;
%
generate_1D_images(sprintf('%s%d.png', image_db_base_dir_2D_crops, image_index_to_slice), ...
    image_db_base_dir, num_slices);

% 3. Remove two random slices from the image db to be later used for
% reconstruction experiments.
%
% Set params:
%
remove_indices = [4, 10];
for i = 1 : length(remove_indices)
   movefile(sprintf('%s%d.png', image_db_base_dir, remove_indices(i)), ...
       sprintf('%s%d.png', test_images_base_dir, remove_indices(i)));
end

% 4. For each 1D slice, compute the pdf hierarcy and visualize (first
%   without smoothing) - for now, we only process one target level (though
%   we compute the hierarchy up to the target level since we use an
%   iterative fine to coarse level computation).
%
% Set params:
%
target_level = 2;                   % Level 1 is the finest level.
pdf_num_bins = 256; 
smooth_kernel_size_rc = [1, 5];     % For 1D inputs, set to [1, size]
smooth_kernel_sigma_rc = 1.0;
smooth_kernel_size_b = [5, 1];
smooth_kernel_sigma_b = 1.0; 
show_expval_images = 'yes';
show_1D_pdf_images = 'yes';
%
pdf_images = SHM_ComputePDFImages(image_db_base_dir, target_level, pdf_num_bins, ...
    smooth_kernel_size_rc, smooth_kernel_sigma_rc, ...
    smooth_kernel_size_b, smooth_kernel_sigma_b, show_expval_images, ... 
    show_1D_pdf_images);


% 4. Use Felix's code to a) learn dictionary (of varying number of kernels), and b) perform reconstruction
%   (on non-noisy signal to simulate compression).
% 5. Share code with Robin and Lorenzo.
% 6. Document experiments: maybe even write one matlab file for each big experiment 
%   so it's easy to redo them (i.e. maybe even on Jupyter).

return

%% Test pdf pyramid computations for a 2D input image.

% PARAMS: set input image
test_images_base_dir = '../data/test_images/';
input_image_fname = '../data/test_images/houses_gray.png';
% input_image_fname = '../data/test_images/barbara_1D.png';

% PARAMS: set parameters for pdf pyramid:
pyramid_num_levels = 4; % set to 1 to compute only finest resolution level.
pyramid_num_bins = 256;
                        
% PARAMS: set or compute spatial and range smoothing kernels
% (set sigma to a very small number (e.g. 0.01) for no smoothing):
smooth_kernel_size_rc = [5, 5];
smooth_kernel_sigma_rc = 1.0;
smooth_kernel_size_b = [5, 1];
smooth_kernel_sigma_b = 1.0; 

spatial_smoothing_kernel = SHM_ComputeGaussianKernel(smooth_kernel_size_rc, smooth_kernel_sigma_rc)
range_smoothing_kernel = SHM_ComputeGaussianKernel(smooth_kernel_size_b, smooth_kernel_sigma_b)

% compute pdf pyramid
pdf_pyramid = SHM_ComputePDFPyramid(input_image_fname, pyramid_num_levels, pyramid_num_bins, ...
                spatial_smoothing_kernel, range_smoothing_kernel);

% compute expected value images of pdf pyramid
expval_pyramid = SHM_ComputeExpectedValuePyramid(pdf_pyramid);

% display expected value images:
for level = 1 : pyramid_num_levels
    imwrite(uint8(expval_pyramid{level,1}), sprintf('%sexpval_level_%d_bins_%d.png', ...
        test_images_base_dir, level, pyramid_num_bins));
end

return

%% Test dictionary learning using CSC:

image_db_dir = '../data/image_dbs/flower_100_100';
num_kernels = 4;
output_base_name = '../experiments/learned_filters/';

%learned_filters = learn_kernels_2D( image_db_dir, num_kernels, output_base_name );

% test csc reconstruction

filters_filename = '../experiments/learned_filters/filters_ours_obj1.06e+04.mat';
test_images_dir = '../experiments/exp1/test_images';
results_base_dir = '../experiments/exp1/';

reconstruct_LMM_2D_sparse(filters_filename, test_images_dir, results_base_dir);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
