%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

clear all;
close all;

addpath('./shm');
addpath('./shm/image_helpers');
addpath('./sparse_coding');
addpath('./sparse_coding/image_helpers');


%% NOTES:
% 1. We follow Matlab's convention for image indexing i.e. an image is a
% matrix with width = number of cols, and height = number of rows. Thus,
% we index and image with r,c: r goes through the rows while c goes through
% the columns.

%% EXPERIMENT 5:
% This experiment uses CSC to learn a dictionary from more than one image 
% inside a single directory. The outputs are stored in a data.mat file
% consisting of d = dictionary, z = feature maps, ...
%
% This tries to answer Question 1 in: https://docs.google.com/document/d/1MoG3cx3QKQjMSQMDgP7A-V9E_FkglKuTFfX47uYI9zQ/edit

%% PARAMETERS:
%
% CSC parameters:
%
keyword = 'barbara512_tiled';                                 % This keyword can be used to uniquely identify an experiment.
skip_learning = 0;                                      % Set to 0 to perform training; 1 to load data.mat from output dir.
full_image_fname = '../data/image_dbs/barbara512/barbara.png';
image_db_base_dir = '../data/image_dbs/barbara512_tiled/';       % Dir with input image/s for learning.
output_root_dir = '../experiments/exp_5/';              % Dir where to write output.
kernel_size_rc = [11, 11];                              % Size of dictionary atoms.
num_kernels = 9;                                       % Number of dictionary atoms.
lambda_residual = 1.0;
lambda = 1.0;
save_iteration_imgs = 1;                                % Set to 1 to save learning iteration images.
tile_size = [128 128];


%% INITIALIZATIONS:
%
% Create output dirs:
output_base_dir = strcat(output_root_dir, sprintf('%s_d%d_%dx%d_lr%f_l%f/', keyword, num_kernels, ...
    kernel_size_rc(1), kernel_size_rc(2), lambda_residual, lambda));

if ~exist(output_root_dir, 'dir')
        mkdir(output_root_dir);        
end

if ~exist(output_base_dir, 'dir')
        mkdir(output_base_dir);        
end

copy_of_input_base_dir = sprintf('%s/input', output_base_dir); 
if ~exist(copy_of_input_base_dir, 'dir')
        mkdir(copy_of_input_base_dir);        
end

%% CROPPING IMAGE:
% This experiment prepares a database of 2D images from one image.
%
% Note that if you already have a database of 2D images, 
% set skip_setup_2D = 1 (otherwise set skip_setup_2D = 0).
%
skip_setup_2D = 0;

%% 1.1. Here, we prepare a database of 2D images by 
% slicing an input image. 
%
if ~skip_setup_2D
    
    if ~exist(image_db_base_dir, 'dir')
        mkdir(image_db_base_dir);        
    end
    
    generate_image_crops(full_image_fname, image_db_base_dir, tile_size(1, 1), tile_size(1, 2));
end

% Copy input images to output dir:
image_db_fnames = dir(image_db_base_dir);
for i = 3 : length(image_db_fnames) % Here we start with index 3 because 1 and 2 correspond to dirs: ., and ..

    copyfile(sprintf('%s/%s', image_db_fnames(i).folder, image_db_fnames(i).name), ...
        sprintf('%s/%s', copy_of_input_base_dir, image_db_fnames(i).name));
end


%% ADMM LEARNING OR LOAD PRECOMPUTED DATA:
%
out_fname = sprintf('%s/data.mat', output_base_dir);
if ~skip_learning
    [dictionary, fmaps, reconst, obj, iterations, orig_imgs, image_names] = learn_kernels_2D(image_db_base_dir, ...
        output_base_dir, save_iteration_imgs, kernel_size_rc, num_kernels, lambda_residual, lambda);
    
    % Save learning output to file:
    save(out_fname, 'dictionary', 'fmaps', 'reconst', 'obj', 'iterations', 'orig_imgs', 'image_names');

else 
    % Load data from file:
    load(out_fname, 'dictionary');
    load(out_fname, 'fmaps');
end


%% ANALYSIS / VISUALIZATIONS:
%
% Start interactive visualization ui.

interactive({out_fname}); 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
