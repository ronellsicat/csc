%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

clear all;
close all;

addpath('./shm');
addpath('./shm/image_helpers');
addpath('./sparse_coding');
addpath('./sparse_coding/image_helpers');

%% NOTES:
% 1. We follow Matlab's convention for image indexing i.e. an image is a
% matrix with width = number of cols, and height = number of rows. Thus,
% we index and image with r,c: r goes through the rows while c goes through
% the columns.
%
% 2. We refer to the range domain as bin and use b for indexing.

%% EXPERIMENT 2:
% This experiment prepares a database of 1D images and their corresponding 
% 2D pdf image representations. It then uses CSC to learn a
% dictionary which can reconstruct any 2D pdf image. Each pdf image is
% represented in the form of a type double matrix of size [bins, rows,
% cols].
%
% Note that if you already have a database of 2D pdf images, 
% set skip_setup = 1 (otherwise set skip_setup = 0).
%
skip_setup = 0;

%% 1.1. Here, we prepare a database of 1D images by 
% slicing an input image. 
%
% Set params:
%
full_image_fname = '../data/test_images/barbara_crop_1.png';
image_db_base_dir = '../data/image_dbs/barbara_1_128/';
num_slices = 16;
%
if ~skip_setup
    
    if ~exist(image_db_base_dir, 'dir')
        mkdir(image_db_base_dir);        
    end
    
    generate_1D_images(full_image_fname, image_db_base_dir, num_slices)
end

%% 1.2. From each 1D image, we now compute a 2D pdf image. For now, 
% we only process one target level though conceptually we can use any/all
% levels since we compute the hierarchy up to the target level and we use
% an iterative fine to coarse level computation.
%
% Set params:
%
target_level = 2;                   % Level 1 is the finest level.
pdf_num_bins = 256; 
smooth_kernel_size_rc = [1, 5];     % For 1D inputs, set to [1, size]
smooth_kernel_sigma_rc = 1.0;
smooth_kernel_size_b = [9, 1];
smooth_kernel_sigma_b = 2.0; 
show_expval_images = 'no';
show_1D_pdf_images = 'no';
pdf_images_base_dir = '../data/image_dbs/barbara_1_128_pdf/';
%
if ~skip_setup
    pdf_images = SHM_ComputePDFImages(image_db_base_dir, target_level, pdf_num_bins, ...
        smooth_kernel_size_rc, smooth_kernel_sigma_rc, ...
        smooth_kernel_size_b, smooth_kernel_sigma_b, show_expval_images, ... 
        show_1D_pdf_images, pdf_images_base_dir);
end

%% 1.3. Save pdf images as 2D images on disk. PDF values [0,1] are scaled to 
% uint8 [0,255] by matlab. TODO: Fix this scaling.
%
if ~skip_setup
    pdf_images_size = size(pdf_images);
    for i = 1 : pdf_images_size(1)
        cur_pdf_image = pdf_images{i, 1};
        pdf_image_size = size(cur_pdf_image);
        imwrite(double(reshape(cur_pdf_image, [pdf_image_size(1), pdf_image_size(3)])), sprintf('%s%d.png', pdf_images_base_dir, i));
    end
end

%% 2. Remove random images from the image db to be later used for
% reconstruction experiments.
%
% Set params:
%
test_images_base_dir = '../experiments/exp_2/test_images/';
test_indices = [4];
%test_indices = [4, 10];
%
image_db_fnames = dir(pdf_images_base_dir);
image_db_fnames = image_db_fnames(3:end);
%
if ~skip_setup
    
    if ~exist(test_images_base_dir, 'dir')
        mkdir(test_images_base_dir);        
    end
    
    for i = 1 : length(test_indices)
        movefile(sprintf('%s%s', pdf_images_base_dir, image_db_fnames(test_indices(i)).name), ...
           sprintf('%s%s', test_images_base_dir, image_db_fnames(test_indices(i)).name));
    end
else
    fprintf('Skipping set-up. Using images in %s.\n\n', pdf_images_base_dir);
end

%% 3. Use Felix's code to learn dictionary (with specified number of
% kernels). If learning was done previously with output saved to a mat
% file, you can skip learning by setting skip_learning = 1.
% (otherwise set skip_learning = 0)
%
skip_learning = 0;

% Set params:
%
output_base_dir = '../experiments/exp_2/';
kernel_size_rc = [11, 11];
num_kernels = 8;
lambda_residual = 1.0;
lambda = 1.0;
%
if ~skip_learning
    [d, z, Dz, obj, iterations, out_fname] = learn_kernels_2D(pdf_images_base_dir, ...
        output_base_dir, kernel_size_rc, num_kernels, lambda_residual, lambda);
end

%% 4. Using learned dictionary from previous step, perform reconstruction 
% (on noisy and non-noisy signal to simulate compression).
%
% Set params:
%
pct_missing_pixels = 2;   % Set this to 2 so that we don't have any missing pixels. Otherwise set as [0,1].
filters_fname_on_disk = '../experiments/exp_2/filters_ours_obj5.42e+03.mat';
%
if ~skip_learning
    filters_filename = out_fname;
else
    fprintf('Skipped dictionary learning. Using dictionary in %s.\n\n', filters_fname_on_disk);
    filters_filename = filters_fname_on_disk;    
end
%
[feature_maps, reconst_image] = reconstruct_LMM_2D_sparse(filters_filename, test_images_base_dir, ...
    output_base_dir, pct_missing_pixels);

% Display feature maps used in reconstruction and save to disk (for visualization only):
%
plot_feature_maps_sum = 'yes';
feature_maps_sum = compute_feature_maps_sum(feature_maps, plot_feature_maps_sum);
print(sprintf('%sfmaps_sum_fig', output_base_dir), '-dpng')
imwrite(feature_maps_sum, sprintf('%sfmaps_sum.png', output_base_dir));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
